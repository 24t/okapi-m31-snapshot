/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.verification;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.common.Util;

class BlacklistTB {
	
	private List<BlackTerm> entries;

	public BlacklistTB() {
		reset();
	}
	
	private void reset() {
		entries = new ArrayList<BlackTerm>();
	}

	public void guessAndImport(File file) {
		String ext = Util.getExtension(file.getPath());
		if (ext.equalsIgnoreCase(".xyz")){
			// not supported yet
		} else { // Try tab-delimited
			importTSV(file);
		}
	}
	
	public void removeAll() {
		entries.clear();
	}
	
	public List<BlackTerm> getBlacklistStrings() {
		return entries;
	}
	
	private void importTSV(File file) {
		importBlacklist(new BlacklistReader(), file);
	}
	
	private void importBlacklist(BlacklistReader reader, File file) {
		try {
			reader.open(file);
			while (reader.hasNext()) {				
				BlackTerm bterm = reader.next();
				entries.add(bterm);
			}
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}
}
