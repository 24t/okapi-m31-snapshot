/*
 * ===========================================================================
 * Copyright (C) 2013 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
 * ===========================================================================
 */

package net.sf.okapi.lib.languagetool;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.languagetool.AnalyzedTokenReadings;
import org.languagetool.JLanguageTool;
import org.languagetool.Language;
import org.languagetool.Languages;
import org.languagetool.language.AmericanEnglish;
import org.languagetool.tokenizers.WordTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.okapi.common.LocaleId;

/**
 * Helper methods for working with the LanguageTool API's
 * 
 * @author jimh
 *
 */
public class LanguageToolUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(LanguageToolUtil.class);

	private static ConcurrentMap<String, Language> languageCache = new ConcurrentHashMap<>();

	/**
	 * Thread-safe cache for {@link Language} objects. These objects should be thread safe
	 * and can be reused across {@link JLanguageTool} instances.
	 * @param locale 
	 * @return a cached {@link Language}
	 */
	public static Language getCachedLanguage(LocaleId locale) {
		// do an atomic get, first-check.
		Language language = languageCache.get(locale.toBCP47());
		if (language == null) {
			// There's a good chance we're the first thread to request this
			// settings
			// be optimistic, and assume we are the the only one.
			language = getLanguage(locale);
			// atomic second-check
			Language race = languageCache.putIfAbsent(locale.toBCP47(), language);
			if (race != null) {
				// some other thread managed to do the double-check at the same
				// time as us, and win the race to the putIfAbsent
				// use the winner's Connector, (and abandon ours).
				if (language instanceof Closeable) {
					try {
						((Closeable)language).close();
						language = null;
					} catch (Exception e) {
						LOGGER.error("Language couldn't be closed. Possible memory leak.", locale.toBCP47(), e);
					}
				}
				// return the winner
				return race;
			}
		}
		LOGGER.debug("LanguageTool Language cache hit for {}", locale.toBCP47());
		return language;
	}

	/**
	 * Gets the LT language code for the given locale Id
	 * 
	 * @param locale
	 *            the locale id to map.
	 * @return the LT language code, or null if the locale id could not be
	 *         mapped.
	 */
	public static final Language getLanguage(LocaleId locale) {
		// if the locale is only "en" default to American English
		if (locale == LocaleId.ENGLISH) {
			return new AmericanEnglish();
		}
		
		// try full locale with language, country and possibly a variant code
		if (Languages.isLanguageSupported(locale.toBCP47())) {
			return Languages.getLanguageForLocale(locale.toJavaLocale());
		// else try just the language code
		} else if (Languages.isLanguageSupported(locale.getLanguage())) {
			return Languages.getLanguageForLocale(Locale.forLanguageTag(locale.getLanguage()));
		// otherwise use the DefaultLanguage with universal rules
		} else {
			return new DefaultLanguage();
		}
	}

	/**
	 * Tokenize using LanguageTool tokenizers
	 * 
	 * @param text
	 *            text to tokenize
	 * @param lang
	 *            The {@link Language} from which we get the
	 *            {@link WordTokenizer}
	 * @return list of tokens with all whitespace trimmed
	 * @throws IOException
	 */
	public static final List<AnalyzedTokenReadings> tokenize(String text, Language lang) throws IOException {
		final List<String> tokens = lang.getWordTokenizer().tokenize(text);
		final List<AnalyzedTokenReadings> aTokens = lang.getTagger().tag(tokens);
		return aTokens;
	}
}
