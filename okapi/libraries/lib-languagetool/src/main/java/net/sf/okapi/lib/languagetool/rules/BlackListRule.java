/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.languagetool.rules;

import java.util.List;

import org.languagetool.Language;
import org.languagetool.rules.patterns.PatternRule;
import org.languagetool.rules.patterns.PatternToken;

/**
 * A Blacklist Check pattern rule class. A BlackListRule describes a language error 
 * where a blacklisted target term is found in the target.
 * @author jimh
 */
public class BlackListRule extends PatternRule {

	public BlackListRule(String id, Language language, List<PatternToken> patternTokens, String description,
			String message, String shortMessage) {
		super(id, language, patternTokens, description, message, shortMessage);
	}

	public BlackListRule(PatternRule trgPattern, String shortMessage) {
		super(trgPattern.getId(), trgPattern.getLanguage(), trgPattern.getPatternTokens(), 
				trgPattern.getDescription(), trgPattern.getMessage(), shortMessage);
	}
}
