/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.languagetool.rules;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.languagetool.Language;
import org.languagetool.rules.ITSIssueType;
import org.languagetool.rules.patterns.PatternRule;
import org.languagetool.rules.patterns.PatternToken;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.lib.languagetool.LanguageToolUtil;
import net.sf.okapi.lib.terminology.ConceptEntry;
import net.sf.okapi.lib.terminology.IGlossaryReader;
import net.sf.okapi.lib.terminology.LangEntry;

/**
 * Converts a {@link IGlossaryReader} into a list of {@link TermCheckRule}
 * to be used for term consistency checking.
 * 
 * @author jimh
 *
 */
public class ConceptEntry2TermCheckRules extends AbstractLanguageToolRuleGenerator {
	private static final String DESCRIPTION = "Term Consistency Check";
	private static final String MESSAGE = "The source term \"%s\" is not translated with the recommended target term. <suggestion>%s</suggestion>";
	private static final String SHORT_MESSAGE = "Check Term Translation";

	private LocaleId srcLocale;
	private LocaleId trgLocale;

	// LanguageTool Language objects
	private Language srcLang;
	private Language trgLang;
	private List<TermCheckRule> rules;

	public ConceptEntry2TermCheckRules(LocaleId srcLocale, LocaleId trgLocale) {
		this.srcLocale = srcLocale;
		this.trgLocale = trgLocale;
		this.srcLang = LanguageToolUtil.getCachedLanguage(srcLocale);
		this.trgLang = LanguageToolUtil.getCachedLanguage(trgLocale);
		rules = new ArrayList<>();
	}

	public List<TermCheckRule> convert(List<ConceptEntry> concepts) throws IOException {
		if (concepts == null) {
			throw new OkapiException("ConceptEntry list is null.");
		}
		rules.clear();
		for (ConceptEntry ce : concepts) {
			addRule(ce, srcLocale, trgLocale);					
		}
		return rules;
	}

	public List<TermCheckRule> convert(IGlossaryReader reader) throws IOException {
		if (reader == null) {
			throw new OkapiException("Glossary Reader is null.");
		}
		rules.clear();
		try {
			while (reader.hasNext()) {
				ConceptEntry ce = reader.next();
				addRule(ce, srcLocale, trgLocale);					
			}
		} finally {
			reader.close();
		}

		return rules;
	}
	
	private void addRule(ConceptEntry ce, LocaleId srcLocale, LocaleId trgLocaleId) throws IOException {
		if (ce.hasLocale(srcLocale) && ce.hasLocale(trgLocale)) {
			LangEntry srcLent = ce.getEntries(srcLocale);
			LangEntry trgLent = ce.getEntries(trgLocale);
			if (srcLent.hasTerm() && trgLent.hasTerm()) {
				rules.add(convertToBitextPatternRule(srcLent.getTerm(0).getText(), trgLent.getTerm(0).getText()));
			}
		}
	}

	private TermCheckRule convertToBitextPatternRule(String srcTerm, String trgTerm) throws IOException {
		List<PatternToken> srcTokens = getPatternTokens(srcTerm, srcLang);
		List<PatternToken> trgTokens = getPatternTokens(trgTerm, trgLang);
		PatternRule srcPattern = new PatternRule(srcTerm, srcLang, srcTokens, DESCRIPTION,
				String.format(MESSAGE, srcTerm, trgTerm), SHORT_MESSAGE);
		srcPattern.setLocQualityIssueType(ITSIssueType.Terminology);
		PatternRule trgPattern = new PatternRule(trgTerm, trgLang, trgTokens, DESCRIPTION,
				String.format(MESSAGE, srcTerm, trgTerm), SHORT_MESSAGE);
		TermCheckRule r = new TermCheckRule(srcPattern, trgPattern);
		r.setLocQualityIssueType(ITSIssueType.Terminology);
		return r;
	}

	public Language getSrcLang() {
		return srcLang;
	}

	public Language getTrgLang() {
		return trgLang;
	}
}
