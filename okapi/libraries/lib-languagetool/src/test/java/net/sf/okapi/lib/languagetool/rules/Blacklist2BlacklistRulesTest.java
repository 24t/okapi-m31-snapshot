/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.languagetool.rules;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.lib.verification.BlackTerm;
import net.sf.okapi.lib.verification.BlacklistReader;

public class Blacklist2BlacklistRulesTest {
	private static final LocaleId SPANISH = new LocaleId("es-ES");

	private Blacklist2BlacklistRules converter;
	private Blacklist2BlacklistRules converter2;
	private List<BlackTerm> singleBlackTerm;
	private List<BlackTerm> multipleBlackTerms;

	@Before
	public void setUp() throws Exception {
		converter = new Blacklist2BlacklistRules(SPANISH);
		converter2 = new Blacklist2BlacklistRules(LocaleId.KOREAN);
		
		BlackTerm sbt = new BlackTerm();
		sbt.text = "term1 term2";
		sbt.suggestion = "suggest1 suggest2";
		singleBlackTerm = new ArrayList<>();
		singleBlackTerm.add(sbt);

		BlackTerm mbt1 = new BlackTerm();
		mbt1.text = "term1 term2";
		mbt1.suggestion = "suggest1 suggest2";
		
		BlackTerm mbt2 = new BlackTerm();
		mbt2.text = "s_term1 s_term2";
		mbt2.suggestion = "s_suggest1 s_suggest2";
		
		multipleBlackTerms = new ArrayList<>();
		multipleBlackTerms.add(mbt1);
		multipleBlackTerms.add(mbt2);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void LanguageToolLocales() {
		assertEquals("Spanish", converter.getTrgLang().toString());
		assertEquals("Default", converter2.getTrgLang().toString());
	}

	@Test
	public void singleTermToBitext() throws IOException {
		List<BlackListRule> rules = converter.convert(singleBlackTerm);
		assertTrue(!rules.isEmpty());
		assertTrue(rules.size() == 1);
		BlackListRule r = rules.get(0);
		assertEquals("term1", r.getPatternTokens().get(0).getString());
		assertEquals("term2", r.getPatternTokens().get(1).getString());

		// using Default Language and tokenizer
		rules = converter2.convert(singleBlackTerm);
		assertTrue(!rules.isEmpty());
		assertTrue(rules.size() == 1);
		r = rules.get(0);
		assertEquals("term1", r.getPatternTokens().get(0).getString());
		assertEquals("term2", r.getPatternTokens().get(1).getString());
	}

	@Test
	public void multiTermToBitext() throws IOException {
		List<BlackListRule> rules = converter.convert(multipleBlackTerms);
		assertTrue(!rules.isEmpty());
		assertTrue(rules.size() == 2);
		BlackListRule r = rules.get(0);
		assertEquals("term1", r.getPatternTokens().get(0).getString());
		assertEquals("term2", r.getPatternTokens().get(1).getString());

		r = rules.get(1);
		assertEquals("s_term1", r.getPatternTokens().get(0).getString());
		assertEquals("s_term2", r.getPatternTokens().get(1).getString());
	}

	@Test
	public void blacklistFile() throws IOException {
		try (InputStream is = FileLocation.fromClass(this.getClass()).in("/black_tsv_simple.txt").asInputStream() ) {
			BlacklistReader reader = new BlacklistReader();
			reader.open(is);
			List<BlackListRule> rules = converter.convert(reader);
			assertTrue(!rules.isEmpty());
			assertTrue(rules.size() == 4);
		}
	}
}
