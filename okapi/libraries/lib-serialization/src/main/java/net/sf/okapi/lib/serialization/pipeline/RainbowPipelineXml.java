/*===========================================================================
  Copyright (C) 2009-2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.serialization.pipeline;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.StreamUtil;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.XMLWriter;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.pipeline.IPipeline;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.common.pipeline.Pipeline;

public class RainbowPipelineXml {

	public static final IPipeline read(InputStream inputStream) {
		try {
			DocumentBuilderFactory Fact = DocumentBuilderFactory.newInstance();
			Fact.setValidating(false);

			// Select the type of input
			Document doc;
			doc = Fact.newDocumentBuilder().parse(inputStream);
			NodeList nodes = doc.getElementsByTagName("step");
			Pipeline pipeline = new Pipeline();

			for (int i = 0; i < nodes.getLength(); i++) {
				Node elem = nodes.item(i);
				Node node = elem.getAttributes().getNamedItem("class");
				if (node == null) {
					throw new OkapiException("The attribute 'class' is missing.");
				}
				// Create the class
				// Check if we can use the available steps (and their loaders)
				String className = node.getNodeValue();
				IPipelineStep step;
				step = (IPipelineStep) Class.forName(className, true, Thread.currentThread().getContextClassLoader())
						.newInstance();
				// Load the parameters if needed
				IParameters params = step.getParameters();
				if (params != null) {
					params.fromString(Util.getTextContent(elem));
				}
				// add the step
				pipeline.addStep(step);
			}

			return pipeline;
		} catch (IOException e) {
			throw new OkapiException(e);
		} catch (SAXException e) {
			throw new OkapiException(e);
		} catch (ParserConfigurationException e) {
			throw new OkapiException(e);
		} catch (DOMException e) {
			throw new OkapiException(e);
		} catch (InstantiationException e) {
			throw new OkapiException(e);
		} catch (IllegalAccessException e) {
			throw new OkapiException(e);
		} catch (ClassNotFoundException e) {
			throw new OkapiException(e);
		}
	}

	public static final void write(IPipeline pipeline, OutputStream outputStream) throws IOException {
		XMLWriter writer = null;
		Writer strWriter = null;
		try {
			strWriter = new StringWriter();
			writer = new XMLWriter(strWriter);
			// Force "\n" to avoid conversion of "\r\n" to "\r\r\n" later
			writer.setLineBreak(Util.LINEBREAK_UNIX);
			writer.writeStartDocument();
			writer.writeStartElement("rainbowPipeline");
			writer.writeAttributeString("version", "1");
			for (IPipelineStep step : pipeline.getSteps()) {
				writer.writeStartElement("step");
				writer.writeAttributeString("class", step.getClass().getName());
				IParameters params = step.getParameters();
				if (params != null) {
					writer.writeString(params.toString());
				}
				writer.writeEndElementLineBreak(); // step
			}			
		} finally {
			if (writer != null) {
				writer.writeEndElementLineBreak(); // rainbowPipeline
				writer.writeEndDocument();
				writer.close();
				StreamUtil.copy(new ByteArrayInputStream(strWriter.toString().getBytes(StandardCharsets.UTF_8)), outputStream);				
			}
		}
	}
}
