package net.sf.okapi.lib.serialization.pipeline;

import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.pipeline.IPipeline;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.proto.pipeline.Pipeline;

public final class PipelineToProtoBuffers {
	
	static net.sf.okapi.proto.pipeline.Pipeline toPipeline(IPipeline okapiPipeline) {
		net.sf.okapi.proto.pipeline.Pipeline.Builder pipelineBuilder = net.sf.okapi.proto.pipeline.Pipeline.newBuilder();
		pipelineBuilder.setId(okapiPipeline.getId());
		for (IPipelineStep okapiStep : okapiPipeline.getSteps()) {
			net.sf.okapi.proto.pipeline.PipelineStep.Builder stepBuilder = net.sf.okapi.proto.pipeline.PipelineStep.newBuilder();
			stepBuilder.setName(okapiStep.getName());
			stepBuilder.setDescription(okapiStep.getDescription()); 
			stepBuilder.setStepClass(okapiStep.getClass().getName());
			IParameters p = okapiStep.getParameters();
			if (p != null) {
				stepBuilder.setParams(p.toString());
			}
			stepBuilder.setIsLastOutputStep(okapiStep.isLastOutputStep());
			pipelineBuilder.addSteps(stepBuilder);
		}
		return pipelineBuilder.build();
	}
	
	static byte[] toBytes(IPipeline okapiPipeline) {
		Pipeline p = toPipeline(okapiPipeline);
		return p.toByteArray();
	}
}
