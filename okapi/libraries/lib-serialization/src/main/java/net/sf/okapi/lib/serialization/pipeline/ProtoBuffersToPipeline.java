package net.sf.okapi.lib.serialization.pipeline;

import java.io.IOException;
import java.io.InputStream;

import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.pipeline.IPipeline;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.common.pipeline.Pipeline;

public final class ProtoBuffersToPipeline {

	static IPipeline toPipeline(net.sf.okapi.proto.pipeline.Pipeline protoPipeline)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		IPipeline okapiPipeline = new Pipeline();
		okapiPipeline.setId(protoPipeline.getId());
		for (net.sf.okapi.proto.pipeline.PipelineStep protoStep : protoPipeline.getStepsList()) {
			// FIXME what classloader should we use?
			IPipelineStep okapiStep = (IPipelineStep) Class
					.forName(protoStep.getStepClass(), true, Thread.currentThread().getContextClassLoader())
					.newInstance();
			IParameters p = okapiStep.getParameters();
			if (p != null) {
				p.fromString(protoStep.getParams());
				okapiStep.setParameters(p);
			}
			okapiStep.setLastOutputStep(protoStep.getIsLastOutputStep());
			okapiPipeline.addStep(okapiStep);
		}
		return okapiPipeline;
	}

	static IPipeline toPipeline(InputStream pipeline)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, IOException {
		return toPipeline(net.sf.okapi.proto.pipeline.Pipeline.parseFrom(pipeline));
	}
	
	static IPipeline toPipeline(byte[] pipeline)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, IOException {
		return toPipeline(net.sf.okapi.proto.pipeline.Pipeline.parseFrom(pipeline));
	}
}
