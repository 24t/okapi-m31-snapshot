package net.sf.okapi.lib.serialization.pipeline;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.pipeline.IPipeline;

public class RainbowPipelineXmlTest {
	private InputStream TEST; 

	@Before
	public void setUp() {		
	}

	@After
	public void tearDown() {
	}

	@Test
	public void readXmlPipeline() throws IOException {
		TEST = FileLocation.fromClass(RainbowPipelineXmlTest.class).in("/test.pln").asInputStream();
		IPipeline okapiPipeline = RainbowPipelineXml.read(TEST);
		assertNotNull(okapiPipeline);
		assertTrue(okapiPipeline.getSteps().size() == 2);
		assertEquals("Create Target", okapiPipeline.getSteps().get(0).getName());
		assertEquals("Remove Target", okapiPipeline.getSteps().get(1).getName());
		TEST.close();
	}
	
	@Test
	public void roundTrip() throws IOException {
		TEST = FileLocation.fromClass(RainbowPipelineXmlTest.class).in("/test.pln").asInputStream();
		IPipeline okapiPipeline = RainbowPipelineXml.read(TEST);
		
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		RainbowPipelineXml.write(okapiPipeline, os);
		
		okapiPipeline = RainbowPipelineXml.read(new ByteArrayInputStream(os.toByteArray()));
		assertNotNull(okapiPipeline);
		assertTrue(okapiPipeline.getSteps().size() == 2);
		assertEquals("Create Target", okapiPipeline.getSteps().get(0).getName());
		assertEquals("Remove Target", okapiPipeline.getSteps().get(1).getName());
		TEST.close();
	}

}

