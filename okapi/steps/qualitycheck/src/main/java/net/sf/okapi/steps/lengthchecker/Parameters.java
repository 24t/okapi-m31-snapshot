/*===========================================================================
 Copyright (C) 2016 by the Okapi Framework contributors
 -----------------------------------------------------------------------------
 This library is free software; you can redistribute it and/or modify it 
 under the terms of the GNU Lesser General Public License as published by 
 the Free Software Foundation; either version 2.1 of the License, or (at 
 your option) any later version.

 This library is distributed in the hope that it will be useful, but 
 WITHOUT ANY WARRANTY; without even the implied warranty of 
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License 
 along with this library; if not, write to the Free Software Foundation, 
 Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
 ===========================================================================*/

package net.sf.okapi.steps.lengthchecker;

import net.sf.okapi.common.StringParameters;

public class Parameters extends StringParameters {

	private static final String CHECKSTORAGESIZE = "checkStorageSize";
	private static final String CHECKMAXCHARLENGTH = "checkMaxCharLength";
	private static final String MAXCHARLENGTHBREAK = "maxCharLengthBreak";
	private static final String MAXCHARLENGTHABOVE = "maxCharLengthAbove";
	private static final String MAXCHARLENGTHBELOW = "maxCharLengthBelow";
	private static final String CHECKMINCHARLENGTH = "checkMinCharLength";
	private static final String MINCHARLENGTHBREAK = "minCharLengthBreak";
	private static final String MINCHARLENGTHABOVE = "minCharLengthAbove";
	private static final String MINCHARLENGTHBELOW = "minCharLengthBelow";
	private static final String CHECKABSOLUTEMAXCHARLENGTH = "checkAbsoluteMaxCharLength";
	private static final String ABSOLUTEMAXCHARLENGTH = "absoluteMaxCharLength";

	public Parameters() {
		super();
	}

	public boolean getCheckStorageSize() {
		return getBoolean(CHECKSTORAGESIZE);
	}

	public void setCheckStorageSize(boolean checkStorageSize) {
		setBoolean(CHECKSTORAGESIZE, checkStorageSize);
	}

	public boolean getCheckMaxCharLength() {
		return getBoolean(CHECKMAXCHARLENGTH);
	}

	public void setCheckMaxCharLength(boolean checkMaxCharLength) {
		setBoolean(CHECKMAXCHARLENGTH, checkMaxCharLength);
	}

	public int getMaxCharLengthBreak() {
		return getInteger(MAXCHARLENGTHBREAK);
	}

	public void setMaxCharLengthBreak(int maxCharLengthBreak) {
		setInteger(MAXCHARLENGTHBREAK, maxCharLengthBreak);
	}

	public int getMaxCharLengthAbove() {
		return getInteger(MAXCHARLENGTHABOVE);
	}

	public void setMaxCharLengthAbove(int maxCharLengthAbove) {
		setInteger(MAXCHARLENGTHABOVE, maxCharLengthAbove);
	}

	public int getMaxCharLengthBelow() {
		return getInteger(MAXCHARLENGTHBELOW);
	}

	public void setMaxCharLengthBelow(int maxCharLengthBelow) {
		setInteger(MAXCHARLENGTHBELOW, maxCharLengthBelow);
	}

	public boolean getCheckMinCharLength() {
		return getBoolean(CHECKMINCHARLENGTH);
	}

	public void setCheckMinCharLength(boolean checkMinCharLength) {
		setBoolean(CHECKMINCHARLENGTH, checkMinCharLength);
	}

	public int getMinCharLengthBreak() {
		return getInteger(MINCHARLENGTHBREAK);
	}

	public void setMinCharLengthBreak(int minCharLengthBreak) {
		setInteger(MINCHARLENGTHBREAK, minCharLengthBreak);
	}

	public int getMinCharLengthAbove() {
		return getInteger(MINCHARLENGTHABOVE);
	}

	public void setMinCharLengthAbove(int minCharLengthAbove) {
		setInteger(MINCHARLENGTHABOVE, minCharLengthAbove);
	}

	public int getMinCharLengthBelow() {
		return getInteger(MINCHARLENGTHBELOW);
	}

	public void setMinCharLengthBelow(int minCharLengthBelow) {
		setInteger(MINCHARLENGTHBELOW, minCharLengthBelow);
	}

	public boolean getCheckAbsoluteMaxCharLength() {
		return getBoolean(CHECKABSOLUTEMAXCHARLENGTH);
	}

	public void setCheckAbsoluteMaxCharLength(boolean checkAbsoluteMaxCharLength) {
		setBoolean(CHECKABSOLUTEMAXCHARLENGTH, checkAbsoluteMaxCharLength);
	}

	public int getAbsoluteMaxCharLength() {
		return getInteger(ABSOLUTEMAXCHARLENGTH);
	}

	public void setAbsoluteMaxCharLength(int absoluteMaxCharLength) {
		setInteger(ABSOLUTEMAXCHARLENGTH, absoluteMaxCharLength);
	}

	
	
	@Override
	public void reset() {
		super.reset();		
		setCheckMaxCharLength(true);
		setMaxCharLengthBreak(20);
		setMaxCharLengthAbove(200);
		setMaxCharLengthBelow(350);
		setCheckMinCharLength(true);
		setMinCharLengthBreak(20);
		setMinCharLengthAbove(45);
		setMinCharLengthBelow(30);

		setCheckStorageSize(true);

		setCheckAbsoluteMaxCharLength(false);
		setAbsoluteMaxCharLength(255);

	}

	@Override
	public void fromString(String data) {
		super.fromString(data);
	}

	@Override
	public String toString() {
		return super.toString();
	}
}
