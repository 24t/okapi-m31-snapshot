/*===========================================================================
  Copyright (C) 2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.common.codesimplifier;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;

@EditorFor(PostSegmentationParameters.class)
public class PostSegmentationParameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String REMOVE_LEADING_TRAILING_CODES = "removeLeadingTrailingCodes";
	
	public PostSegmentationParameters() {
		super();
	}
	
	@Override
	public void reset() {
		super.reset();
		setRemoveLeadingTrailingCodes(true);
	}

	public void setRemoveLeadingTrailingCodes(boolean removeLeadingTrailingCodes) {
		setBoolean(REMOVE_LEADING_TRAILING_CODES, removeLeadingTrailingCodes);
	}

	public boolean getRemoveLeadingTrailingCodes() {
		return getBoolean(REMOVE_LEADING_TRAILING_CODES);
	}

	@Override
	public ParametersDescription getParametersDescription() {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(REMOVE_LEADING_TRAILING_CODES,
			"Remove leading and trailing codes",
			"Removes leading and trailing codes from the segment and place them in the inter-segment part.");
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription(ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("Post-segmentation Inline Codes Simplifier", true, false);		
		desc.addCheckboxPart(paramsDesc.get(REMOVE_LEADING_TRAILING_CODES));
		return desc;
	}
}
