/*===========================================================================
  Copyright (C) 2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.searchandreplace;

import java.util.ArrayList;

import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.Util;

public class Parameters extends StringParameters {

	private static final String REGEX = "regEx";
	private static final String DOTALL = "dotAll";
	private static final String IGNORECASE = "ignoreCase";
	private static final String MULTILINE = "multiLine";
	private static final String TARGET = "target";
	private static final String SOURCE = "source";
	private static final String REPLACEALL = "replaceALL";
	private static final String COUNT = "count";
	private static final String REPLACEMENTSPATH = "replacementsPath";
	private static final String LOGPATH = "logPath";
	private static final String SAVELOG = "saveLog";
	
	public boolean getRegEx() {
		return getBoolean(REGEX);
	}

	public void setRegEx(boolean regEx) {
		setBoolean(REGEX, regEx);
	}

	public boolean getDotAll() {
		return getBoolean(DOTALL);
	}

	public void setDotAll(boolean dotAll) {
		setBoolean(DOTALL, dotAll);
	}

	public boolean getIgnoreCase() {
		return getBoolean(IGNORECASE);
	}

	public void setIgnoreCase(boolean ignoreCase) {
		setBoolean(IGNORECASE, ignoreCase);
	}

	public boolean getMultiLine() {
		return getBoolean(MULTILINE);
	}

	public void setMultiLine(boolean multiLine) {
		setBoolean(MULTILINE, multiLine);
	}

	public boolean getTarget() {
		return getBoolean(TARGET);
	}

	public void setTarget(boolean target) {
		setBoolean(TARGET, target);
	}

	public boolean getSource() {
		return getBoolean(SOURCE);
	}

	public void setSource(boolean source) {
		setBoolean(SOURCE, source);
	}

	public boolean getReplaceAll() {
		return getBoolean(REPLACEALL);
	}

	public void setReplaceAll(boolean replaceAll) {
		setBoolean(REPLACEALL, replaceAll);
	}

	public String getReplacementsPath() {
		return getString(REPLACEMENTSPATH);
	}

	public void setReplacementsPath(String replacementsPath) {
		setString(REPLACEMENTSPATH, replacementsPath);
	}

	public String getLogPath() {
		return getString(LOGPATH);
	}

	public void setLogPath(String logPath) {
		setString(LOGPATH, logPath);
	}

	public boolean getSaveLog() {
		return getBoolean(SAVELOG);
	}

	public void setSaveLog(boolean saveLog) {
		setBoolean(SAVELOG, saveLog);
	}

	public void setRules(ArrayList<String[]> rules) {
		this.rules = rules;
	}

	public ArrayList<String[]> rules;

	public Parameters () {
		super();
	}

	public void reset () {
		super.reset();
		setRegEx(false);
		setDotAll(false);
		setIgnoreCase(false);
		setMultiLine(false);
		setTarget(true);
		setSource(false);
		setReplaceAll(true);
		setReplacementsPath("");
		setLogPath(Util.ROOT_DIRECTORY_VAR+"/replacementsLog.txt");
		setSaveLog(false);
		
		rules = new ArrayList<String[]>();
	}

	public void addRule (String pattern[]) {
		rules.add(pattern);
	}	
	
	public ArrayList<String[]> getRules () {
		return rules;
	}	

	public void fromString (String data) {
		super.fromString(data);
		
		int count = buffer.getInteger(COUNT, 0);
		for ( int i=0; i<count; i++ ) {
			String []s = new String[3];
			s[0] = buffer.getString(String.format("use%d", i), "");
			s[1] = buffer.getString(String.format("search%d", i), "");
			s[2] = buffer.getString(String.format("replace%d", i), "");
			rules.add(s);
		}
	}

	public String toString() {
		buffer.setInteger(COUNT, rules.size());
		int i = 0;

		for ( String[] temp : rules ) {
			buffer.setString(String.format("use%d", i), temp[0]);
			buffer.setString(String.format("search%d", i), temp[1]);
			buffer.setString(String.format("replace%d", i), temp[2]);
			i++;
		}		
		return buffer.toString();
	}
}
