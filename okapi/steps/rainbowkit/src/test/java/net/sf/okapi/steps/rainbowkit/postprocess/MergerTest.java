/*===========================================================================
"#, fuzzy\r""#, fuzzy\r"  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.rainbowkit.postprocess;

import static org.junit.Assert.assertEquals;
import net.sf.okapi.common.filterwriter.GenericContent;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.resource.TextUnitUtil;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class MergerTest {
	
	private GenericContent fmt = new GenericContent();

	@Test
	public void testTransfer () {
		// source: <i>Before</i> <b>bold<i> after.
		// target: <b>XXXXXX</b> <i>XXXX</i> XXXX.
		TextFragment tf = new TextFragment("s1");
		tf.append(TagType.OPENING, "i", "<i>", 1);
		tf.append("s2");
		tf.append(TagType.CLOSING, "i", "</i>", 1);
		tf.append(" ");
		tf.append(TagType.OPENING, "b", "<b>");
		tf.append("s3");
		tf.append(TagType.OPENING, "i", "<i>");
		tf.append("s3");
		assertEquals("s1<i>s2</i> <b>s3<i>s3", tf.toText());
		assertEquals("s1<1>s2</1> <b2/>s3<b3/>s3", fmt.setContent(tf).toString());
		TextContainer srcOri = new TextContainer(tf);
		
		tf = new TextFragment("s1");
		tf.append(TagType.OPENING, "b", "<b>", 1);
		tf.append("s2");
		tf.append(TagType.CLOSING, "b", "</b>", 1);
		tf.append(" ");
		tf.append(TagType.OPENING, "i", "<i>");
		tf.append("s3");
		tf.append(TagType.CLOSING, "i", "</i>");
		tf.append("s3");
		assertEquals("s1<b>s2</b> <i>s3</i>s3", tf.toText());
		assertEquals("s1<1>s2</1> <2>s3</2>s3", fmt.setContent(tf).toString());
		TextContainer trgTra = new TextContainer(tf);
		
		TextFragment res = TextUnitUtil.copySrcCodeDataToMatchingTrgCodes(srcOri.getFirstContent(),
			trgTra.getFirstContent(), true, true, null, null);
		assertEquals("s1<i>s2</i> <b>s3</i>s3<i>", res.toText());
		assertEquals("s1<1>s2</1> <2>s3</2>s3<3>", fmt.setContent(res).toString());
		//TODO: not the result we want
    }

}
