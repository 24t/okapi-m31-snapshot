/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.rainbowkit.xliff;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;

public class XLIFF2Options extends StringParameters implements IEditorDescriptionProvider {

	private static final String WITHORIGINALDATA = "withOriginalData"; //$NON-NLS-1$
	private static final String CREATETIPPACKAGE = "createTipPackage"; //$NON-NLS-1$
	private static final String ELIMINATEEMPTYTARGETSWITHNONEMPTYSOURCE = "eliminateEmptyTargetsWithNonEmptySource"; //$NON-NLS-1$
	
	public XLIFF2Options () {
		super();
	}
	
	@Override
	public void reset() {
		super.reset();
		setWithOriginalData(true);
		setCreateTipPackage(false);
	}

	public boolean getwithOriginalData () {
		return getBoolean(WITHORIGINALDATA);
	}

	public void setWithOriginalData (boolean withOriginalData) {
		setBoolean(WITHORIGINALDATA, withOriginalData);
	}
	
	public boolean getCreateTipPackage () {
		return getBoolean(CREATETIPPACKAGE);
	}
	
	public void setCreateTipPackage (boolean createTipPackage) {
		setBoolean(CREATETIPPACKAGE, createTipPackage);
	}
	
	public boolean getEliminateEmptyTargetsWithNonEmptySource () {
		return getBoolean(ELIMINATEEMPTYTARGETSWITHNONEMPTYSOURCE);
	}
	
	public void setEliminateEmptyTargetsWithNonEmptySource (boolean eliminateEmptyTargetsWithNonEmptySource) {
		setBoolean(ELIMINATEEMPTYTARGETSWITHNONEMPTYSOURCE, eliminateEmptyTargetsWithNonEmptySource);
	}

	@Override
	public ParametersDescription getParametersDescription() {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(WITHORIGINALDATA, "Output includes original data when available", null);
		desc.add(CREATETIPPACKAGE, "Create a TIPP file", null);
		desc.add(ELIMINATEEMPTYTARGETSWITHNONEMPTYSOURCE, "Remove segments with empty target and non-empty source", null);
		
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription(ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("Experimental XLIFF 2.0", true, false);
		desc.addCheckboxPart(paramsDesc.get(WITHORIGINALDATA));
		desc.addCheckboxPart(paramsDesc.get(ELIMINATEEMPTYTARGETSWITHNONEMPTYSOURCE));
		//TODO maybe desc.addCheckboxPart(paramsDesc.get(CREATETIPPACKAGE));
		return desc;
	}

}
