/*
 * ===========================================================================
 * Copyright (C) 2013 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
 * ===========================================================================
 */

package net.sf.okapi.steps.languagetool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Locale;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.annotation.GenericAnnotation;
import net.sf.okapi.common.annotation.GenericAnnotationType;
import net.sf.okapi.common.annotation.ITSLQIAnnotations;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.lib.terminology.ConceptEntry;
import net.sf.okapi.lib.verification.BlackTerm;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.languagetool.Languages;

@RunWith(JUnit4.class)
public class LanguageToolTest {

	private LanguageTool lt;
	private final LocaleId srcLoc = new LocaleId("en-us");
	private final LocaleId trgLoc = new LocaleId("fr-fr");

	@Before
	public void setUp () throws IOException {
		lt = new LanguageTool(null, srcLoc, trgLoc);
	}
	
	@After
	public void tearDown () {
		lt.shutDown();
	}

	@Test
	public void simpleTest1 () {
		ITextUnit tu = new TextUnit("id", "original teext");
		tu.setTargetContent(trgLoc, new TextFragment("texte original"));
		lt.run(tu);
		ITSLQIAnnotations anns = tu.getSource().getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(2, anns.getAllAnnotations().size());
		assertEquals("typographical", anns.getFirstAnnotation(GenericAnnotationType.LQI).getString(GenericAnnotationType.LQI_TYPE));
		anns = tu.getTarget(trgLoc).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
		assertEquals("typographical", anns.getFirstAnnotation(GenericAnnotationType.LQI).getString(GenericAnnotationType.LQI_TYPE));
	}

	@Test
	public void testLTLanguages () {
		assertEquals("en-US", Languages.getLanguageForLocale(new Locale("en", "US")).getShortNameWithCountryAndVariant());
		assertEquals("en-US", Languages.getLanguageForShortName("en-US").getShortNameWithCountryAndVariant());
		assertEquals("en-US", Languages.getLanguageForLocale(new Locale("en", "us")).getShortNameWithCountryAndVariant());
		assertEquals("en-US", Languages.getLanguageForShortName("en-us").getShortNameWithCountryAndVariant());
	}
	
	@Test
	public void simpleTest2 () {
		ITextUnit tu = new TextUnit("id", "File not found: %1");
		tu.setTargetContent(trgLoc, new TextFragment("Fichier non trouv\u00e9: %1"));
		lt.run(tu);
		ITSLQIAnnotations anns = tu.getSource().getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
		anns = tu.getTarget(trgLoc).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
	}

	@Test
	public void testWithCodes () {
		TextFragment tf = new TextFragment();
		tf.append(TagType.OPENING, "b", "<b>");
		tf.append("This is a short sentence.");
		tf.append(TagType.CLOSING, "b", "</b>");
		ITextUnit tu = new TextUnit("id");
		tu.setSourceContent(tf);
		tu.setTargetContent(trgLoc, tf.clone());
		lt.run(tu);
		ITSLQIAnnotations anns = tu.getSource().getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
//TODO: fix inline handling		
		GenericAnnotation ann = anns.getAnnotations(GenericAnnotationType.LQI).get(0);
		assertEquals(0, (int)ann.getInteger(GenericAnnotationType.LQI_XSTART));
		assertEquals(7, (int)ann.getInteger(GenericAnnotationType.LQI_XEND));
	}
	
	@Test
	public void termCheckTbxWithEmptyLists() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(true);
		p.setBlacklistCheck(true);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setTermCheckPath(null);
	    p.setBlacklistCheckPath(null);
		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.fromString("hu"), new LinkedList<ConceptEntry>(), new LinkedList<BlackTerm>());
		
		// term check rule will fire as the target term is not exactly as it should be
		ITextUnit tu = new TextUnit("id", "Alpha smoothing factor");
		tu.setTargetContent(LocaleId.fromString("hu"), new TextFragment("tényező simítási Alfax"));
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.fromString("hu")).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
	}
	
	@Test
	public void termCheckTbx() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(true);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setTermCheckPath(FileLocation.fromClass(this.getClass()).in("/test01.tbx").asPath().toAbsolutePath().toString());
		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.fromString("hu"));
		
		// term check rule will fire as the target term is not exactly as it should be
		ITextUnit tu = new TextUnit("id", "Alpha smoothing factor");
		tu.setTargetContent(LocaleId.fromString("hu"), new TextFragment("tényező simítási Alfax"));
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.fromString("hu")).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
		
		// no rule should fire, exact match
		ITextUnit tu2 = new TextUnit("id2", "Alpha smoothing factor");
		tu2.setTargetContent(LocaleId.fromString("hu"), new TextFragment("Alfa simítási tényező"));
		langTool.run(tu2);
		anns = tu2.getTarget(LocaleId.fromString("hu")).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
		
		// no rule should fire, stemming difference only
		ITextUnit tu3 = new TextUnit("id3", "Alpha smooth factors");
		tu3.setTargetContent(LocaleId.fromString("hu"), new TextFragment("Alfa simítási tényező"));
		langTool.run(tu3);
		anns = tu3.getTarget(LocaleId.fromString("hu")).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
	}
	
	@Test
	public void termCheckSdlTbx() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(true);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setTermCheckPath(FileLocation.fromClass(this.getClass()).in("/sdl_tbx.tbx").asPath().toAbsolutePath().toString());
		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.GERMAN);
		
		// morphological variants match for both english and german
		// "typeface family" and "Schriftfamilie"
		ITextUnit tu = new TextUnit("id", "The typeface families define all fonts.");
		tu.setTargetContent(LocaleId.GERMAN, new TextFragment("Die Schriftfamilien definieren alle Schriften."));
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.GERMAN).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
		
		// A literal translation "font group" = "Schriftgruppen" is caught as an error
		ITextUnit tu2 = new TextUnit("id", "The typeface families define all fonts.");
		tu2.setTargetContent(LocaleId.GERMAN, new TextFragment("Die Schriftgruppen definieren alle Schriften."));
		langTool.run(tu2);
		anns = tu2.getTarget(LocaleId.GERMAN).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
	}
	
	@Test
	public void termCheckCsv() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(true);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setTermCheckPath(FileLocation.fromClass(this.getClass()).in("/test01.csv").asPath().toAbsolutePath().toString());
		
		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.FRENCH);
		
		ITextUnit tu = new TextUnit("id", "This is a sentence with source 1.");
		tu.setTargetContent(LocaleId.FRENCH, new TextFragment("This is a sentence without the target term."));
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.FRENCH).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
		
		// no rule should fire, exact match
		ITextUnit tu2 = new TextUnit("id2", "This is a sentence with source 1.");
		tu2.setTargetContent(LocaleId.FRENCH, new TextFragment("This is a sentence with target 1."));
		langTool.run(tu2);
		anns = tu2.getTarget(LocaleId.FRENCH).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
		
		// no rule should fire
		ITextUnit tu3 = new TextUnit("id3", "This is a sentence with source 3.");
		tu3.setTargetContent(LocaleId.FRENCH, new TextFragment("This is a sentence with target 3."));
		langTool.run(tu3);
		anns = tu3.getTarget(LocaleId.FRENCH).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
	}
	
	@Test
	public void termCheckBlacklist() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(false);
		p.setBlacklistCheck(true);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setBlacklistCheckPath(FileLocation.fromClass(this.getClass()).in("/black_tsv_simple.txt").asPath().toAbsolutePath().toString());
		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.FRENCH);
		
		ITextUnit tu = new TextUnit("id", "This is a sentence with the blackterm.");
		tu.setTargetContent(LocaleId.FRENCH, new TextFragment("This is a sentence with the blackterm1."));
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.FRENCH).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
		
		// no rule should fire, no blackterms
		ITextUnit tu2 = new TextUnit("id2", "This is a sentence with source 1.");
		tu2.setTargetContent(LocaleId.FRENCH, new TextFragment("This is a sentence with target 1."));
		langTool.run(tu2);
		anns = tu2.getTarget(LocaleId.FRENCH).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
		
		// another blackterm
		tu = new TextUnit("id", "This is a sentence with another blackterm.");
		tu.setTargetContent(LocaleId.FRENCH, new TextFragment("This is a sentence with the blackterm4."));
		langTool.run(tu);
		anns = tu.getTarget(LocaleId.FRENCH).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
	}
	
	@Test
	public void termCheckBlacklistJapanese() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(false);
		p.setBlacklistCheck(true);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setBlacklistCheckPath(FileLocation.fromClass(this.getClass()).in("/black_tsv_simple_JA.txt").asPath().toAbsolutePath().toString());
		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.JAPANESE);
		
		// target contains Japanese blackterm "だけ"
		ITextUnit tu = new TextUnit("id", "Srcwrd Srcwrd Srcwrd Srcwrd. ");
		tu.setTargetContent(LocaleId.JAPANESE, new TextFragment("私はあなただけを愛しています。"));
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.JAPANESE).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
	}
}
