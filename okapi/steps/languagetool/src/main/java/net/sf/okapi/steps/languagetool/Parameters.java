/*
 * ===========================================================================
 * Copyright (C) 2013 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
 * ===========================================================================
 */

package net.sf.okapi.steps.languagetool;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.CheckboxPart;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.PathInputPart;

@EditorFor(Parameters.class)
public class Parameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String ENABLEFALSEFRIENDS = "enableFalseFriends";
	private static final String CHECKSOURCE = "checkSource";
	private static final String CHECKSPELLING = "checkSpelling";
	private static final String CHECKGRAMMAR = "checkGrammar";
	private static final String TERMCHECK = "termCheck";
	private static final String BLACKLISTCHECK = "blacklistCheck";
	private static final String TERMCHECKPATH = "termCheckPath";
	private static final String BLACKLISTCHECKPATH = "blacklistCheckPath";

	public Parameters () {
		super();
	}

	@Override
	public void reset () {
		super.reset();
		setCheckSource(true);
		setEnableFalseFriends(true);
		setCheckSpelling(true);
		setCheckGrammar(true);
		setTermCheck(false);
		setBlacklistCheck(false);
		setTermCheckPath(null);
		setBlacklistCheckPath(null);
	}

	public boolean getCheckSource () {
		return getBoolean(CHECKSOURCE);
	}
	
	public void setCheckSource (boolean checkSource) {
		setBoolean(CHECKSOURCE, checkSource);
	}

	public boolean getEnableFalseFriends () {
		return getBoolean(ENABLEFALSEFRIENDS);
	}
	
	public void setEnableFalseFriends (boolean enableFalseFriends) {
		setBoolean(ENABLEFALSEFRIENDS, enableFalseFriends);
	}

	public boolean getCheckSpelling () {
		return getBoolean(CHECKSPELLING);
	}
	
	public void setCheckSpelling (boolean checkSpelling) {
		setBoolean(CHECKSPELLING, checkSpelling);
	}
	
	public boolean getCheckGrammar () {
		return getBoolean(CHECKGRAMMAR);
	}
	
	public void setCheckGrammar (boolean checkGrammer) {
		setBoolean(CHECKGRAMMAR, checkGrammer);
	}
	
	public boolean getTermCheck () {
		return getBoolean(TERMCHECK);
	}
	
	public void setTermCheck (boolean termCheck) {
		setBoolean(TERMCHECK, termCheck);
	}
	
	public boolean getBlacklistCheck () {
		return getBoolean(BLACKLISTCHECK);
	}
	
	public void setBlacklistCheck (boolean blacklistCheck) {
		setBoolean(BLACKLISTCHECK, blacklistCheck);
	}
	
	public String getBlacklistCheckPath () {
		return getString(BLACKLISTCHECKPATH);
	}
	
	public void setBlacklistCheckPath (String blacklistPath) {
		setString(BLACKLISTCHECKPATH, blacklistPath);
	}
	
	public String getTermCheckPath () {
		return getString(TERMCHECKPATH);
	}
	
	public void setTermCheckPath (String termPath) {
		setString(TERMCHECKPATH, termPath);
	}

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(CHECKSOURCE, "Check also the source text (in addition to the target)", null);
		desc.add(ENABLEFALSEFRIENDS, "Check for false friends", null);
		desc.add(CHECKSPELLING, "Check for spelling mistakes", null);
		desc.add(CHECKGRAMMAR, "Check for Grammar/Style mistakes", null);
		desc.add(TERMCHECK, "Check for term consistency errors", null);
		desc.add(BLACKLISTCHECK, "Check for black listed terms", null);
		desc.add(TERMCHECKPATH, "Path to terminology file (tbx|csv|tsv)", null);
		desc.add(BLACKLISTCHECKPATH, "Path to black list file", null);
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription (ParametersDescription paramDesc) {
		EditorDescription desc = new EditorDescription("LanguageTool", true, false);
		desc.addTextLabelPart("Spelling/Grammar Checks");
		desc.addCheckboxPart(paramDesc.get(CHECKSOURCE));
		desc.addCheckboxPart(paramDesc.get(ENABLEFALSEFRIENDS));
		desc.addCheckboxPart(paramDesc.get(CHECKSPELLING));
		desc.addCheckboxPart(paramDesc.get(CHECKGRAMMAR));
		desc.addSeparatorPart();
		desc.addTextLabelPart("Terminology Checks");
		CheckboxPart cb = desc.addCheckboxPart(paramDesc.get(TERMCHECK));
		PathInputPart p = desc.addPathInputPart(paramDesc.get(TERMCHECKPATH), "Select Terminology File...", false);
		p.setAllowEmpty(true);
		p.setBrowseFilters("TBX (*.tbx)\tTable (*.csv;*.tsv)\tAll Files (*.*)", "*.tbx\t*.csv;*.tsv\t*.*");
		p.setMasterPart(cb, true);
		cb = desc.addCheckboxPart(paramDesc.get(BLACKLISTCHECK));
		p = desc.addPathInputPart(paramDesc.get(BLACKLISTCHECKPATH), "Select Black List File...", false);
		p.setAllowEmpty(true);
		p.setBrowseFilters("Tab Delimited (*.tsv;*.txt)\tAll Files (*.*)", "*.tsv;*.txt\t*.*");
		p.setMasterPart(cb, true);
		return desc;
	}
}
