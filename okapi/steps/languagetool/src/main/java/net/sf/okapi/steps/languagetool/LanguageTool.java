/*
 * ===========================================================================
 * Copyright (C) 2013 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
 * ===========================================================================
 */

package net.sf.okapi.steps.languagetool;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.annotation.GenericAnnotation;
import net.sf.okapi.common.annotation.ITSLQIAnnotations;
import net.sf.okapi.common.annotation.IssueAnnotation;
import net.sf.okapi.common.annotation.IssueType;
import net.sf.okapi.common.exceptions.OkapiIOException;
import net.sf.okapi.common.resource.ISegments;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.lib.languagetool.LanguageToolUtil;
import net.sf.okapi.lib.languagetool.rules.BlackListRule;
import net.sf.okapi.lib.languagetool.rules.Blacklist2BlacklistRules;
import net.sf.okapi.lib.languagetool.rules.ConceptEntry2TermCheckRules;
import net.sf.okapi.lib.languagetool.rules.TermCheckRule;
import net.sf.okapi.lib.terminology.ConceptEntry;
import net.sf.okapi.lib.terminology.IGlossaryReader;
import net.sf.okapi.lib.terminology.csv.CSVReader;
import net.sf.okapi.lib.terminology.tbx.TBXReader;
import net.sf.okapi.lib.terminology.tsv.TSVReader;
import net.sf.okapi.lib.verification.BlackTerm;
import net.sf.okapi.lib.verification.BlacklistReader;

import org.languagetool.AnalyzedSentence;
import org.languagetool.JLanguageTool;
import org.languagetool.Languages;
import org.languagetool.MultiThreadedJLanguageTool;
import org.languagetool.rules.Rule;
import org.languagetool.rules.RuleMatch;
import org.languagetool.rules.bitext.BitextRule;
import org.languagetool.tools.Tools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LanguageTool {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private final Parameters params;
	private final LocaleId srcLoc;
	private final LocaleId trgLoc;
	private MultiThreadedJLanguageTool srcLt;
	private MultiThreadedJLanguageTool trgLt;
	private List<BitextRule> bitextRules;
	private List<TermCheckRule> termCheckRules;
	private List<BlackListRule> blackListRules;
	
	/**
	 * Creates a LanguageTool object with a given set of options.
	 * 
	 * @param params
	 *            the options to assign to this object (use null for the
	 *            defaults).
	 * @throws IOException 
	 */
	public LanguageTool(Parameters params, LocaleId sourceLocale, LocaleId targetLocale) throws IOException {
		this.params = (params == null ? new Parameters() : params);
		bitextRules = new ArrayList<>();
		this.srcLoc = sourceLocale;
		this.trgLoc = targetLocale;
		srcLt = startInstance(sourceLocale);
		trgLt = startInstance(trgLoc);
		termCheckRules = new ArrayList<>();
		blackListRules = new ArrayList<>();		

		try {
			bitextRules = Tools.getBitextRules(srcLt.getLanguage(), trgLt.getLanguage());
		} catch (Throwable e) {
			LOGGER.warn("Cannot load bi-text rules ({}).\nNo bi-lingual rules for the target will be checked.",
					e.getMessage());
		}
		
		if (!Util.isEmpty(this.params.getTermCheckPath())) {
			// load terminology and convert to rules
			ConceptEntry2TermCheckRules converter = new ConceptEntry2TermCheckRules(srcLoc, trgLoc);
			File file = new File(this.params.getTermCheckPath());
			IGlossaryReader reader = createGloassaryReader(file, srcLoc, trgLoc);
			try {
				reader.open(file);
				termCheckRules = converter.convert(reader);
			} finally {
				reader.close();
			}
		}
		
		if (!Util.isEmpty(this.params.getBlacklistCheckPath())) {
			// load terminology and convert to rules
			Blacklist2BlacklistRules converter = new Blacklist2BlacklistRules(trgLoc);
			File file = new File(this.params.getBlacklistCheckPath());
			BlacklistReader reader = new BlacklistReader();
			try {
				reader.open(file);
				blackListRules = converter.convert(reader);
			} finally {
				reader.close();
			}
		}
		
		LOGGER.debug("LT Languages available: " + Languages.get().toString());
	}
	
	/**
	 * Load term and blackterm rules from in-memory lists rather than files
	 * @param params
	 * @param sourceLocale
	 * @param targetLocale
	 * @param terms
	 * @param blackTerms
	 * @throws IOException
	 */
	public LanguageTool(Parameters params, LocaleId sourceLocale, LocaleId targetLocale, 
			List<ConceptEntry> terms, List<BlackTerm> blackTerms) throws IOException {
		this(params, sourceLocale, targetLocale);
		termCheckRules = (new ConceptEntry2TermCheckRules(sourceLocale, targetLocale)).convert(terms);
		blackListRules = (new Blacklist2BlacklistRules(targetLocale)).convert(blackTerms);
	}

	public boolean isInitialized() {
		return !(srcLt == null || trgLt == null);
	}

	public void shutDown() {
		if (srcLt != null) {
			srcLt.shutdown();
			srcLt = null;
		}

		if (trgLt != null) {
			trgLt.shutdown();
			trgLt = null;
		}
	}

	private MultiThreadedJLanguageTool startInstance(LocaleId locId) {
		try {
			MultiThreadedJLanguageTool lt = new MultiThreadedJLanguageTool(LanguageToolUtil.getCachedLanguage(locId), 4);
			LOGGER.info("Using LT language '{}' ('{}') for locale '{}'",
					lt.getLanguage().getShortNameWithCountryAndVariant(), lt.getLanguage().getName(), locId.toString());
			return lt;
		} catch (Throwable e) {
			throw new OkapiIOException("Cannot create or initialize the LanguageTool object.\n" + e.getMessage());
		}
	}

	/**
	 * Performs the proofreading of the text unit according to user selected
	 * options.
	 * 
	 * @param tu the unit to process.
	 */
	public void run(ITextUnit tu) {
		// Filter out text units we don't need to check
		if (!tu.isTranslatable())
			return;
		if (tu.isEmpty())
			return;

		try {
			TextContainer srcTc = tu.getSource();
			TextContainer trgTc = tu.getTarget(trgLoc);
			ISegments srcSegs = srcTc.getSegments();
			ISegments trgSegs = null;
			if (trgTc != null)
				trgSegs = trgTc.getSegments();

			List<RuleMatch> srcMatches = null;
			List<RuleMatch> trgMatches = null;
			for (Segment srcSeg : srcSegs) {

				TextFragment srcTf = null;
				TextFragment trgTf = null;

				if (trgSegs != null) {
					Segment trgSeg = trgSegs.get(srcSeg.getId());
					if (trgSeg != null) {
						trgTf = trgSeg.getContent();
						trgMatches = check(srcSeg.getContent().getCodedText(), trgTf.getCodedText());
					}
				}

				if (params.getCheckSource()) {
					srcTf = srcSeg.getContent();
					srcMatches = srcLt.check(srcTf.getCodedText(), true, JLanguageTool.ParagraphHandling.ONLYNONPARA);
				}

				// Attach the results
				if (trgMatches != null) {
					for (RuleMatch match : trgMatches) {
						ITSLQIAnnotations.addAnnotations(trgTc, createAnnotation(match, trgTf, true, srcSeg.getId()));
					}
				}
				if (srcMatches != null) {
					for (RuleMatch match : srcMatches) {
						ITSLQIAnnotations.addAnnotations(srcTc, createAnnotation(match, srcTf, false, srcSeg.getId()));
					}
				}
			}
		} catch (Throwable e) {
			throw new OkapiIOException("Error while checking. " + e.getMessage());
		}
	}

	private List<RuleMatch> check(String src, String trg) throws IOException {
		final AnalyzedSentence srcText = srcLt.getAnalyzedSentence(src);
		final AnalyzedSentence trgText = trgLt.getAnalyzedSentence(trg);
		List<Rule> nonBitextRules = trgLt.getAllRules();
		for (Rule rule : nonBitextRules) {
			rule.reset();
			
			if (!params.getCheckSpelling()) {
				if (rule.isDictionaryBasedSpellingRule()) {
					rule.setDefaultOff();
				}
			}
			
			if (!params.getCheckGrammar()) {
				// if its not a spelling rule we assume a style/grammar rule
				if (!rule.isDictionaryBasedSpellingRule()) {
					rule.setDefaultOff();
				}
			}
		}
		final List<RuleMatch> ruleMatches = trgLt.checkAnalyzedSentence(JLanguageTool.ParagraphHandling.ONLYNONPARA,
				nonBitextRules, 0, 0, 1, trg, trgText, null);
		ruleMatches.addAll(applyBiTextRules(srcText, trgText, trg));
		
		if (params.getTermCheck()) {
			ruleMatches.addAll(applyTermCheckRules(srcText, trgText, trg));
		}
		
		if (params.getBlacklistCheck()) {
			ruleMatches.addAll(applyBlacklistRules(trgText, trg));			
		}
		return ruleMatches;
	}
	
	private List<RuleMatch> applyBiTextRules(AnalyzedSentence srcText, AnalyzedSentence trgText, String trg) throws IOException {
		List<RuleMatch> ruleMatches = new ArrayList<>();
		for (BitextRule bRule : bitextRules) {
			final RuleMatch[] curMatch = bRule.match(srcText, trgText);
			if (curMatch != null && curMatch.length > 0) {
				// adjust positions for bitext rules
				for (RuleMatch match : curMatch) {
					if (match.getColumn() < 0) {
						match.setColumn(1);
					}
					if (match.getEndColumn() < 0) {
						match.setEndColumn(trg.length() + 1); // we count from 0
					}
					if (match.getLine() < 0) {
						match.setLine(1);
					}
					if (match.getEndLine() < 0) {
						match.setEndLine(1);
					}
					ruleMatches.add(match);
				}
			}
		}
		return ruleMatches;
	}
	
	private IGlossaryReader createGloassaryReader(File file, LocaleId srcLoc, LocaleId trgLoc) {
		String ext = Util.getExtension(file.getPath());
		if ( ext.equalsIgnoreCase(".tbx") ) {
			return new TBXReader();
		}
		if ( ext.equalsIgnoreCase(".csv") ) {
			return new CSVReader(srcLoc, trgLoc);
		}
		else { // Try tab-delimited
			return new TSVReader(srcLoc, trgLoc);
		}
	}
	
	private List<RuleMatch> applyTermCheckRules(AnalyzedSentence srcText, AnalyzedSentence trgText, String trg) throws IOException {
		List<RuleMatch> ruleMatches = new ArrayList<>();
		for (TermCheckRule tcr : termCheckRules) {
			final RuleMatch[] curMatch = tcr.match(srcText, trgText);
			if (curMatch != null && curMatch.length > 0) {
				// adjust positions for term check rules
				for (RuleMatch match : curMatch) {
					if (match.getColumn() < 0) {
						match.setColumn(1);
					}
					if (match.getEndColumn() < 0) {
						match.setEndColumn(trg.length() + 1); // we count from 0
					}
					if (match.getLine() < 0) {
						match.setLine(1);
					}
					if (match.getEndLine() < 0) {
						match.setEndLine(1);
					}
					ruleMatches.add(match);
				}
			}
		}
		return ruleMatches;
	}
	
	private List<RuleMatch> applyBlacklistRules(AnalyzedSentence trgText, String trg) throws IOException {
		List<RuleMatch> ruleMatches = new ArrayList<>();
		for (BlackListRule blr : blackListRules) {
			final RuleMatch[] curMatch = blr.match(trgText);
			if (curMatch != null && curMatch.length > 0) {
				// adjust positions for term check rules
				for (RuleMatch match : curMatch) {
					if (match.getColumn() < 0) {
						match.setColumn(1);
					}
					if (match.getEndColumn() < 0) {
						// we count from 0
						match.setEndColumn(trg.length() + 1); 
					}
					if (match.getLine() < 0) {
						match.setLine(1);
					}
					if (match.getEndLine() < 0) {
						match.setEndLine(1);
					}
					ruleMatches.add(match);
				}
			}
		}
		return ruleMatches;
	}

	/**
	 * Creates an LQI annotation based on the match and context.
	 * 
	 * @param match
	 *            the LT rule match.
	 * @param onTarget
	 *            true if the issue was found on the target content.
	 * @param segId
	 *            segment id.
	 * @return the new annotation, or null if none was created.
	 */
	private GenericAnnotation createAnnotation(RuleMatch match, TextFragment tf, boolean onTarget, String segId) {
		// Check if we skip them or if there is text (if not skip it)
		if (!tf.hasText()) {
			return null;
		}
		
		String msg = match.getMessage();
		// Process the message for tags
		if (msg.indexOf('&') > -1) {
			msg = msg.replace("&lt;suggestion>", "\"");
			msg = msg.replace("&lt;/suggestion>", "\"");
			msg = msg.replace("&amp;", "&");
		}
		
		if (msg.indexOf("<suggestion>") > -1) {
			msg = msg.replace("<suggestion>", "\"");
			msg = msg.replace("</suggestion>", "\"");
			msg = msg.replace("&amp;", "&");
		}
		
		List<String> suggs = match.getSuggestedReplacements();
		if (!Util.isEmpty(suggs)) {
			StringBuilder sb = new StringBuilder(" [Suggestions: ");
			boolean first = true;
			for (String sugg : suggs) {
				if (first)
					first = false;
				else
					sb.append(", ");
				sb.append("\"" + sugg + "\"");
			}
			sb.append("]");
			msg += sb.toString();
		}
		// Create the entry
		// (with adjusted position if needed)
		IssueAnnotation ia = new IssueAnnotation(IssueType.LANGUAGETOOL_ERROR, msg, 2, segId,
				// start in source
				(onTarget ? -1 : TextFragment.fromFragmentToString(tf, match.getFromPos())),
				// end in source
				(onTarget ? -1 : TextFragment.fromFragmentToString(tf, match.getToPos())), 				
				 // start in target
				(onTarget ? TextFragment.fromFragmentToString(tf, match.getFromPos()) : -1),				
				// end in target
				(onTarget ? TextFragment.fromFragmentToString(tf, match.getToPos()) : -1), 
				null);
		ia.setITSType(match.getRule().getLocQualityIssueType().toString());
		return ia;
	}
}
