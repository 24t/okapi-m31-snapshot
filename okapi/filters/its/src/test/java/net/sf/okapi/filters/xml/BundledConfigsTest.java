/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.nio.file.Paths;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.TestUtil;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;

@RunWith(JUnit4.class)
public class BundledConfigsTest {

	private XMLFilter filter;
	private String root;
	private LocaleId locEN = LocaleId.fromString("en");

	@Before
	public void setUp() {
		filter = new XMLFilter();
		root = TestUtil.getParentDir(this.getClass(), "/test01.xml");
	}

	@Test
	public void testAndroidUntranslatable() throws Exception {
		IParameters parameters = filter.getParameters();
		parameters.load(Paths.get(root, "okf_xml@AndroidStrings.fprm").toUri().toURL(), false);
		RawDocument rd = new RawDocument(Paths.get(root, "AndroidTest2.xml").toUri(), "UTF-8", locEN);
		List<Event> events = FilterTestDriver.getEvents(filter, rd, parameters);

		// Test file has 3 TUs but one is marked translatable=false, so make
		// sure it isn't extracted.
		assertTrue(events.get(0).isStartDocument());
		assertEquals("Hello, Android! I am a string resource!", events.get(1).getTextUnit().getSource().toString());
		assertEquals("Hello, Android", events.get(2).getTextUnit().getSource().toString());
		assertTrue(events.get(3).isEndDocument());
		assertEquals(4, events.size());
	}
}
