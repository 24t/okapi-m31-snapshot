/*===========================================================================
  Copyright (C) 2009-2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.idml.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.TestUtil;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filters.InputDocument;
import net.sf.okapi.common.filters.RoundTripComparison;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.filters.idml.IDMLFilter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class IDMLFilterTest {

	private IDMLFilter filter;
	private String root;
	private LocaleId locEN = LocaleId.fromString("en");

	@Before
	public void setUp() {
		filter = new IDMLFilter();
		root = TestUtil.getParentDir(this.getClass(), "/Test01.idml");
	}

	@Test
	public void testDefaultInfo () {
		assertNotNull(filter.getParameters());
		assertNotNull(filter.getName());
		List<FilterConfiguration> list = filter.getConfigurations();
		assertNotNull(list);
		assertTrue(list.size()>0);
	}
	
	@Test
	public void testSimpleEntry () {
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(root+"helloworld-1.idml"), 1);
		assertNotNull(tu);
		String text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
		assertEquals("Hello World!", text);
	}

    @Test
    public void testWhitespaces() {

        List<ITextUnit> iTextUnits = FilterTestDriver.filterTextUnits(getEvents(root +
                "tabsAndWhitespaces.idml"));
        assertNotNull(iTextUnits);
        assertEquals(7, iTextUnits.size());

        TextUnit tu = (TextUnit) iTextUnits.get(0);
        String text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
        assertEquals("Hello World."
                + "Hello\tWorld with a Tab."
                + "Hello \tWorld with a Tab and a white space.", text);


        tu = (TextUnit) iTextUnits.get(1);
        assertTrue(tu.preserveWhitespaces());
        text = TextFragment.getText(tu.getSource().getFirstContent().getText());
        assertEquals("Hello World\t.Hello World.", text);

        tu = (TextUnit) iTextUnits.get(2);
        assertTrue(tu.preserveWhitespaces());
        text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
        assertEquals("Hello      World.", text);

        tu = (TextUnit) iTextUnits.get(3);
        assertTrue(tu.preserveWhitespaces());
        text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
        assertEquals(" Hello World\t.", text);

        tu = (TextUnit) iTextUnits.get(4);
        assertFalse(tu.preserveWhitespaces());
        text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
        assertEquals("HelloWorldwithout.", text);

        tu = (TextUnit) iTextUnits.get(5);
        assertTrue(tu.preserveWhitespaces());
        text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
        assertEquals("Hello \tWorld with a Tab and a white space.", text);

        tu = (TextUnit) iTextUnits.get(6);
        assertTrue(tu.preserveWhitespaces());
        text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
        assertEquals("m-space\u2003here."
                + "n-space\u2002here."
                + "another m-spacehere." // the \u2009 is removed and changed to a code
                //                          TagType.PLACEHOLDER, "sp-thin"
                + "another one\u00a0here.", text);
    }

    @Test
    public void testNewline() {
        ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(root + "newline.idml"), 1);
        assertNotNull(tu);
        TextFragment firstContent = tu.getSource().getFirstContent();
        String text = TextFragment.getText(firstContent.getCodedText());
        assertEquals("32Hello World", text);
    }

	@Test
	public void testSimpleEntry2 () {
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(root+"Test00.idml"), 1);
		assertNotNull(tu);
		String text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
		assertEquals("Hello World!", text);
	}

	@Test
	public void testStartDocument () {
		assertTrue("Problem in StartDocument", FilterTestDriver.testStartDocument(filter,
			new InputDocument(root+"Test01.idml", null),
			"UTF-8", locEN, locEN));
	}
	
	@Test
	public void testDoubleExtraction () {
		// Read all files in the data directory
		ArrayList<InputDocument> list = new ArrayList<InputDocument>();
		
//		list.add(new InputDocument(root+"Test03.idml", "okf_idml@ExtractAll-BreakOnBR.fprm"));
		
		list.add(new InputDocument(root+"Test00.idml", "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root+"Test01.idml", "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root+"Test02.idml", "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root+"helloworld-1.idml", "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root+"ConditionalText.idml", "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root+"Test03.idml", "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root+"testWithSpecialChars.idml", "okf_idml@ExtractAll.fprm"));

		list.add(new InputDocument(root+"TextPathTest01.idml", "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root+"TextPathTest02.idml", "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root+"TextPathTest03.idml", "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root+"TextPathTest04.idml", "okf_idml@ExtractAll.fprm"));
		
		list.add(new InputDocument(root+"idmltest.idml", "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root+"idmltest.idml", null));

		RoundTripComparison rtc = new RoundTripComparison(false); // Do not compare skeleton
		assertTrue(rtc.executeCompare(filter, list, "UTF-8", locEN, locEN, "output"));
	}

	private ArrayList<Event> getEvents (String path) {
		return FilterTestDriver.getEvents(filter, new RawDocument(new File(path).toURI(), "UTF-8", locEN), null);
	}

}
