/*===========================================================================
  Copyright (C) 2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.mif;

import net.sf.okapi.common.ISimplifierRulesParameters;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.filters.InlineCodeFinder;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.core.simplifierrules.ParseException;
import net.sf.okapi.core.simplifierrules.SimplifierRules;

public class Parameters extends StringParameters implements ISimplifierRulesParameters {

	private static final String EXTRACTBODYPAGES = "extractBodyPages";
	private static final String EXTRACTREFERENCEPAGES = "extractReferencePages";
	private static final String EXTRACTMASTERPAGES = "extractMasterPages";
	private static final String EXTRACTHIDDENPAGES = "extractHiddenPages";
	private static final String USECODEFINDER = "useCodeFinder";
	private static final String CODEFINDERRULES = "codeFinderRules";
	private static final String EXTRACTVARIABLES = "extractVariables";
	private static final String EXTRACTINDEXMARKERS = "extractIndexMarkers";
	private static final String EXTRACTLINKS = "extractLinks";
	
	private InlineCodeFinder codeFinder; // Initialized in reset()

	public Parameters () {
		super();
	}
	
	public boolean getUseCodeFinder () {
		return getBoolean(USECODEFINDER);
	}

	public void setUseCodeFinder (boolean useCodeFinder) {
		setBoolean(USECODEFINDER, useCodeFinder);
	}

	public InlineCodeFinder getCodeFinder () {
		return codeFinder;
	}

	public String getCodeFinderData () {
		return codeFinder.toString();
	}

	public void setCodeFinderData (String data) {
		codeFinder.fromString(data);
	}
	
	public boolean getExtractReferencePages () {
		return getBoolean(EXTRACTREFERENCEPAGES);
	}
	
	public void setExtractReferencePages (boolean extractReferencePages) {
		setBoolean(EXTRACTREFERENCEPAGES, extractReferencePages);
	}

	public boolean getExtractMasterPages () {
		return getBoolean(EXTRACTMASTERPAGES);
	}
	
	public void setExtractMasterPages (boolean extractMasterPages) {
		setBoolean(EXTRACTMASTERPAGES, extractMasterPages);
	}
	
	public boolean getExtractHiddenPages () {
		return getBoolean(EXTRACTHIDDENPAGES);
	}

	public void setExtractHiddenPages (boolean extractHiddenPages) {
		setBoolean(EXTRACTHIDDENPAGES, extractHiddenPages);
	}

	public boolean getExtractBodyPages () {
		return getBoolean(EXTRACTBODYPAGES);
	}

	public void setExtractBodyPages (boolean extractBodyPages) {
		setBoolean(EXTRACTBODYPAGES, extractBodyPages);
	}

	public boolean getExtractVariables () {
		return getBoolean(EXTRACTVARIABLES);
	}
	
	public void setExtractVariables (boolean extractVariables) {
		setBoolean(EXTRACTVARIABLES, extractVariables);
	}
	
	public boolean getExtractIndexMarkers () {
		return getBoolean(EXTRACTINDEXMARKERS);
	}
	
	public void setExtractIndexMarkers (boolean extractIndexMarkers) {
		setBoolean(EXTRACTINDEXMARKERS, extractIndexMarkers);
	}
	
	public boolean getExtractLinks () {
		return getBoolean(EXTRACTLINKS);
	}
	
	public void setExtractLinks (boolean extractLinks) {
		setBoolean(EXTRACTLINKS, extractLinks);
	}
	
	@Override
	public String getSimplifierRules() {
		return getString(SIMPLIFIERRULES);
	}

	@Override
	public void setSimplifierRules(String rules) {
		setString(SIMPLIFIERRULES, rules);		
	}

	@Override
	public void validateSimplifierRules() throws ParseException {
		SimplifierRules r = new SimplifierRules(getSimplifierRules(), new Code());
		r.parse();
	}
	
	@Override
	public void reset () {
		super.reset();
		setExtractBodyPages(true);
		setExtractMasterPages(true);
		setExtractReferencePages(true);
		setExtractHiddenPages(true);
		setExtractVariables(true);
		setExtractIndexMarkers(true);
		setExtractLinks(false);
		setUseCodeFinder(true);
		
		codeFinder = new InlineCodeFinder();
		codeFinder.setSample("text <$varName> text");
		codeFinder.setUseAllRulesWhenTesting(true);
		codeFinder.addRule("<\\$.*?>");
		setSimplifierRules(null);
	}

	@Override
	public void fromString (String data) {
		super.fromString(data);
		codeFinder.fromString(buffer.getGroup(CODEFINDERRULES, ""));
	}
	
	@Override
	public String toString () {
		buffer.setGroup(CODEFINDERRULES, codeFinder.toString());
		return super.toString();
	}

}
