package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;

/**
 * Provides a nameable interface.
 */
public interface Nameable {

    /**
     * Gets a qualified name.
     *
     * @return A qualified name
     */
    QName getName();
}
