package net.sf.okapi.filters.openxml;

import static net.sf.okapi.filters.openxml.CodeTypeFactory.createCodeType;

class RunCode {

    private int codeId;
    private String codeType;
    private RunProperties runProperties;

    RunCode(int nextCodeId, RunProperties runProperties, RunProperties combinedRunProperties) {
        this.codeId = nextCodeId;
        this.codeType = createCodeType(combinedRunProperties);
        this.runProperties = runProperties;
    }

    int getCodeId() {
        return codeId;
    }

    String getCodeType() {
        return codeType;
    }

    RunProperties getRunProperties() {
        return runProperties;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + codeId + ", " + codeType + ", " + runProperties + ")";
    }
}