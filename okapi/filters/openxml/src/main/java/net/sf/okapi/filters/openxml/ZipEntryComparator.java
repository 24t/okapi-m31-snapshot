package net.sf.okapi.filters.openxml;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.zip.ZipEntry;

class ZipEntryComparator implements Comparator<ZipEntry> {
	private List<String> names = Arrays.asList(new String[] {
			OpenXMLZipFile.CONTENT_TYPES_PART,
			OpenXMLZipFile.ROOT_RELS_PART,
		});
	ZipEntryComparator() {
	}
	ZipEntryComparator(List<String> additionalPartNames) {
		names = new ArrayList<String>(names); // Old one was immutable
		names.addAll(additionalPartNames);
	}
	@Override
	public int compare(ZipEntry o1, ZipEntry o2) {
		int index1 = names.indexOf(o1.getName());
		int index2 = names.indexOf(o2.getName());
		if (index1 == -1) index1 = Integer.MAX_VALUE;
		if (index2 == -1) index2 = Integer.MAX_VALUE;
		if (index1 < index2)
			return -1;
		else if (index1 > index2)
			return 1;
		else
			return 0;
	}
}