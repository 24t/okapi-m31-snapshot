package net.sf.okapi.filters.openxml;


import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.StartElement;

class StartElementContext {

    private StartElement startElement;
    private XMLEventReader eventReader;
    private XMLEventFactory eventFactory;
    private ConditionalParameters conditionalParameters;
    private Class<? extends ElementSkipper.SkippableElement> skippableElementType;

    StartElementContext(StartElement startElement,
                        XMLEventReader eventReader,
                        XMLEventFactory eventFactory,
                        ConditionalParameters conditionalParameters,
                        Class<? extends ElementSkipper.SkippableElement> skippableElementType) {
        this.startElement = startElement;
        this.eventReader = eventReader;
        this.eventFactory = eventFactory;
        this.conditionalParameters = conditionalParameters;
        this.skippableElementType = skippableElementType;
    }

    StartElement getStartElement() {
        return startElement;
    }

    XMLEventReader getEventReader() {
        return eventReader;
    }

    XMLEventFactory getEventFactory() {
        return eventFactory;
    }

    ConditionalParameters getConditionalParameters() {
        return conditionalParameters;
    }

    Class<? extends ElementSkipper.SkippableElement> getSkippableElementType() {
        return skippableElementType;
    }
}
