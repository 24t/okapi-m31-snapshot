package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.exceptions.OkapiUnexpectedRevisionException;
import net.sf.okapi.filters.openxml.ElementSkipper.InlineSkippableElement;
import net.sf.okapi.filters.openxml.ElementSkipper.RevisionInlineSkippableElement;
import net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static net.sf.okapi.filters.openxml.ElementSkipper.BookmarkElementSkipper.isBookmarkEndElement;
import static net.sf.okapi.filters.openxml.ElementSkipper.BookmarkElementSkipper.isBookmarkStartElement;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionInlineSkippableElement.RUN_INSERTED_CONTENT;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.WPML_ID;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.WPML_NAME;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.getAttributeValue;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isEndElement;

/**
 * Provides an element skipper strategy.
 */
abstract class ElementSkipperStrategy {

    protected Set<String> skippableElementValues;

    protected ElementSkipperStrategy(Set<String> skippableElementValues) {
        this.skippableElementValues = skippableElementValues;
    }

    abstract boolean isSkippableElement(StartElement startElement);

    abstract void skipElement(StartElementContext startElementContext) throws XMLStreamException;

    /**
     * Provides a general element skipper strategy.
     */
    static class GeneralElementSkipperStrategy extends ElementSkipperStrategy {

        private static final Set<String> REVISION_SKIPPABLE_ELEMENTS = new HashSet<>();

        static {
            REVISION_SKIPPABLE_ELEMENTS.addAll(RevisionInlineSkippableElement.getValues());
            REVISION_SKIPPABLE_ELEMENTS.addAll(RevisionPropertySkippableElement.getValues());
        }

        GeneralElementSkipperStrategy(Set<String> skippableElementValues) {
            super(skippableElementValues);
        }

        static void skipElementEvents(StartElementContext startElementContext) throws XMLStreamException {

            if (!startElementContext.getConditionalParameters().getAutomaticallyAcceptRevisions()
                    && REVISION_SKIPPABLE_ELEMENTS.contains(startElementContext.getStartElement().getName().getLocalPart())) {
                throw new OkapiUnexpectedRevisionException();
            }

            if (InlineSkippableElement.class == startElementContext.getSkippableElementType()
                    && RUN_INSERTED_CONTENT.getValue().equals(startElementContext.getStartElement().getName().getLocalPart())) {
                return;
            }

            while (startElementContext.getEventReader().hasNext()) {
                XMLEvent e = startElementContext.getEventReader().nextEvent();

                if (isEndElement(e, startElementContext.getStartElement())) {
                    return;
                }
            }

            throw new IllegalStateException(ExceptionMessages.UNEXPECTED_STRUCTURE);
        }

        @Override
        boolean isSkippableElement(StartElement startElement) {
            return skippableElementValues.contains(startElement.getName().getLocalPart());
        }

        @Override
        void skipElement(StartElementContext startElementContext) throws XMLStreamException {
            skipElementEvents(startElementContext);
        }
    }

    /**
     * Provides a bookmark element skipper strategy.
     */
    static class BookmarkElementSkipperStrategy extends GeneralElementSkipperStrategy {

        private String bookmarkName;
        private String bookmarkId;

        BookmarkElementSkipperStrategy(Set<String> skippableElementValues, String bookmarkName) {
            super(skippableElementValues);
            this.bookmarkName = bookmarkName;
        }

        @Override
        boolean isSkippableElement(StartElement startElement) {
            return isBookmarkStartElement(startElement) && bookmarkName.equals(getAttributeValue(startElement, WPML_NAME))
                    || isBookmarkEndElement(startElement) && Objects.equals(bookmarkId, getAttributeValue(startElement, WPML_ID));
        }

        @Override
        void skipElement(StartElementContext startElementContext) throws XMLStreamException {
            super.skipElement(startElementContext);

            if (isBookmarkStartElement(startElementContext.getStartElement())) {
                bookmarkId = getAttributeValue(startElementContext.getStartElement(), WPML_ID);
                return;
            }

            bookmarkId = null;
        }
    }
}
