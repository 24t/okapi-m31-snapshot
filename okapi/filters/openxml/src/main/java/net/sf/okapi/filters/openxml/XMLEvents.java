package net.sf.okapi.filters.openxml;

import java.util.List;

import javax.xml.stream.events.XMLEvent;

/**
 * Interface for things that consist of a sequence of XMLEvent objects.
 */
public interface XMLEvents {

	public List<XMLEvent> getEvents();

}
