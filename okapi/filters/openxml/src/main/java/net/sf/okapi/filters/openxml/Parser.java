package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLStreamException;

interface Parser<T> {
    T parse() throws XMLStreamException;
}
