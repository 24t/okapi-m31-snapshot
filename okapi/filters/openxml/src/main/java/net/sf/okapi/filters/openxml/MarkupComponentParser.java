package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;
import static net.sf.okapi.filters.openxml.BlockPropertiesFactory.createBlockProperties;
import static net.sf.okapi.filters.openxml.BlockPropertyFactory.createBlockProperty;
import static net.sf.okapi.filters.openxml.StartElementContextFactory.createStartElementContext;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_RUN_PROPERTIES;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isEndElement;

/**
 * Provides a markup component parser.
 */
class MarkupComponentParser {

    static MarkupComponent parseEmptyElementMarkupComponent(XMLEventReader eventReader, XMLEventFactory eventFactory, StartElement startElement) throws XMLStreamException {
        if (!eventReader.hasNext()) {
            throw new IllegalStateException(ExceptionMessages.UNEXPECTED_STRUCTURE);
        }

        XMLEvent nextEvent = eventReader.nextEvent();

        if (!isEndElement(nextEvent, startElement)) {
            throw new IllegalStateException(ExceptionMessages.UNEXPECTED_STRUCTURE);
        }


        return MarkupComponentFactory.createEmptyElementMarkupComponent(eventFactory, startElement, nextEvent.asEndElement());
    }

    static BlockProperties parseBlockProperties(StartElementContext startElementContext, ElementSkipper elementSkipper) throws XMLStreamException {
        List<BlockProperty> properties = new ArrayList<>();

        while (startElementContext.getEventReader().hasNext()) {
            XMLEvent event = startElementContext.getEventReader().nextEvent();

            if (isEndElement(event, startElementContext.getStartElement())) {
                return createBlockProperties(startElementContext.getEventFactory(), startElementContext.getStartElement(), event.asEndElement(), properties);
            }

            if (!event.isStartElement()) {
                continue;
            }

            if (elementSkipper.canBeSkipped(event.asStartElement())) {
                // skip the first level skippable properties
                elementSkipper.skipElement(createStartElementContext(event.asStartElement(), startElementContext));
                continue;
            }
            BlockProperty property = createBlockProperty(gatherEventsWithRevisionPropertiesSkipper(createStartElementContext(event.asStartElement(), startElementContext), elementSkipper));

            if (isEmptyBlockRunPropertiesElement(property)) {
                continue;
            }
            properties.add(property);
        }

        throw new IllegalStateException(ExceptionMessages.UNEXPECTED_STRUCTURE);
    }

    private static boolean isEmptyBlockRunPropertiesElement(BlockProperty blockProperty) {
        return LOCAL_RUN_PROPERTIES.equals(blockProperty.getName().getLocalPart())
                && 2 == blockProperty.getEvents().size()
                && !blockProperty.getEvents().get(0).asStartElement().getAttributes().hasNext();
    }

    private static List<XMLEvent> gatherEventsWithRevisionPropertiesSkipper(StartElementContext startElementContext, ElementSkipper elementSkipper) throws XMLStreamException {
        List<XMLEvent> events = new ArrayList<>();

        events.add(startElementContext.getStartElement());

        while (startElementContext.getEventReader().hasNext()) {
            XMLEvent event = startElementContext.getEventReader().nextEvent();

            if (event.isStartElement() && elementSkipper.canBeSkipped(event.asStartElement())) {
                // skip the second level skippable properties
                elementSkipper.skipElement(createStartElementContext(event.asStartElement(), startElementContext));
                continue;
            }
            events.add(event);

            if (isEndElement(event, startElementContext.getStartElement())) {
                return events;
            }
        }

        throw new IllegalStateException(ExceptionMessages.UNEXPECTED_STRUCTURE);
    }
}
