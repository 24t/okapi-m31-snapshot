package net.sf.okapi.filters.openxml;

import java.util.List;

/**
 * Provides a clarifiable attribute.
 */
class ClarifiableAttribute {

    private String prefix;
    private String name;
    private List<String> values;

    ClarifiableAttribute(String prefix, String name, List<String> values) {
        this.prefix = prefix;
        this.name = name;
        this.values = values;
    }

    String getPrefix() {
        return prefix;
    }

    String getName() {
        return name;
    }

    List<String> getValues() {
        return values;
    }
}
