package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;

public interface XMLFactories {
	XMLInputFactory getInputFactory();
	XMLOutputFactory getOutputFactory();
	XMLEventFactory getEventFactory();
}
