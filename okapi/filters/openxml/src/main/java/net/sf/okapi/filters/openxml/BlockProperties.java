package net.sf.okapi.filters.openxml;


import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import static net.sf.okapi.filters.openxml.XMLEventHelpers.PARAGRAPH_STYLE;

/**
 * Provides a block property markup component.
 */
class BlockProperties extends MarkupComponent implements Nameable {
    private XMLEventFactory eventFactory;
    private StartElement startElement;
    private EndElement endElement;

    private List<Attribute> attributes = new ArrayList<>();
    private List<BlockProperty> properties = new ArrayList<>();

    BlockProperties(XMLEventFactory eventFactory, StartElement startElement, EndElement endElement, List<BlockProperty> properties) {
        this.eventFactory = eventFactory;
        this.startElement = startElement;
        this.endElement = endElement;

        Iterator iterator = startElement.getAttributes();

        while (iterator.hasNext()) {
            attributes.add((Attribute) iterator.next());
        }

        this.properties.addAll(properties);
    }

    @Override
    public QName getName() {
        return startElement.getName();
    }

    @Override
    public List<XMLEvent> getEvents() {
        List<XMLEvent> events = new ArrayList<>();

        events.add(eventFactory.createStartElement(startElement.getName(), getAttributes().iterator(), startElement.getNamespaces()));

        for (BlockProperty property : properties) {
            events.addAll(property.getEvents());
        }
        events.add(endElement);

        return events;
    }

    List<Attribute> getAttributes() {
        return attributes;
    }

    List<BlockProperty> getProperties() {
        return properties;
    }

    BlockProperty getParagraphStyleProperty() {
        for (BlockProperty property : properties) {
            if (PARAGRAPH_STYLE.equals(property.getName())) {
                return property;
            }
        }

        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BlockProperties that = (BlockProperties) o;

        return Objects.equals(startElement, that.startElement)
                && Objects.equals(endElement, that.endElement)
                && Objects.equals(attributes, that.attributes)
                && Objects.equals(properties, that.properties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startElement, endElement, attributes, properties);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + properties.size() + ")" + properties;
    }
}
