package net.sf.okapi.filters.openxml;

public class ExclusionHelper {
    private ExclusionHelper() {
    }

    static boolean isExcluded(String style, ConditionalParameters params) {
        // If style is null or list of excluded styles is empty, nothing should be excluded:
        if (null == style || null == params.tsExcludeWordStyles || params.tsExcludeWordStyles.isEmpty()) {
            return false;
        }

        return params.tsExcludeWordStyles.contains(style);
    }
}