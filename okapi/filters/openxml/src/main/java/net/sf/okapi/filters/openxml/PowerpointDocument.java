package net.sf.okapi.filters.openxml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;

import javax.xml.stream.XMLStreamException;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Drawing;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Powerpoint;

import static net.sf.okapi.filters.openxml.ContentTypes.Types.Common.PACKAGE_RELATIONSHIPS;
import static net.sf.okapi.filters.openxml.ContentTypes.Types.Common.CORE_PROPERTIES_TYPE;

class PowerpointDocument extends DocumentType {
	private List<String> slideNames = new ArrayList<String>();
	private List<String> slideLayoutNames = new ArrayList<String>();
	private static final Pattern RELS_NAME_PATTERN = Pattern.compile(".+slide\\d+\\.xml\\.rels");

	private Matcher relsNameMatcher = RELS_NAME_PATTERN.matcher("").reset();

	private static final String SLIDE_LAYOUT_REL =
			Namespaces.DocumentRelationships.getDerivedURI("/slideLayout");

	PowerpointDocument(OpenXMLZipFile zipFile,
			ConditionalParameters params) {
		super(zipFile, params);
	}

	@Override
	boolean isClarifiablePart(String contentType) {
		return Powerpoint.MAIN_DOCUMENT_TYPE.equals(contentType);
	}

	@Override
	boolean isStyledTextPart(String entryName, String type) {
		if (type.equals(Powerpoint.SLIDE_TYPE)) return true;
		if (type.equals(Drawing.DIAGRAM_TYPE)) return true;
		if (getParams().getTranslatePowerpointMasters()) {
			if (type.equals(Powerpoint.MASTERS_TYPE)) return true;
			// Layouts are translatable if we are translating masters and this particular layout is
			// in use by a slide
			if (type.equals(Powerpoint.LAYOUT_TYPE) && slideLayoutNames.contains(entryName)) return true;
		}
		if (getParams().getTranslatePowerpointNotes() && type.equals(Powerpoint.NOTES_TYPE)) return true;
		return false;
	}

	@Override
	boolean isRelationshipsPart(String entryName, String type) {
		return type.equals(PACKAGE_RELATIONSHIPS);
	}

	@Override
	void initialize() throws IOException, XMLStreamException {
		setStyleDefinitions(StyleDefinitions.emptyStyleDefinitions());
		slideNames = findSlides();
		slideLayoutNames = findSlideLayouts(slideNames);
	}

	@Override
	OpenXMLPartHandler getHandlerForFile(ZipEntry entry, String contentType) {
		relsNameMatcher.reset(entry.getName());
		if (isRelationshipsPart(entry.getName(), contentType) && relsNameMatcher.matches() && getParams().getExtractExternalHyperlinks()) {
			return new RelationshipsPartHandler(getParams(), getZipFile(), entry);
		}

		// Check to see if this is non-translatable
		if (!isTranslatableType(entry.getName(), contentType)) {
			if (isClarifiablePart(contentType)) {
				return new ClarifiablePartHandler(getZipFile(), entry);
			}

			return new NonTranslatablePartHandler(getZipFile(), entry);
		}

		if (isStyledTextPart(entry.getName(), contentType)) {
			return new StyledTextPartHandler(getParams(), getZipFile(), entry, getStyleDefinitions());
		}
		OpenXMLContentFilter openXMLContentFilter = new OpenXMLContentFilter(getParams());
		openXMLContentFilter.setPartName(entry.getName());

		ParseType parseType = ParseType.MSPOWERPOINT;
		if (Powerpoint.SLIDE_TYPE.equals(contentType)) {
			openXMLContentFilter.setBInMainFile(true);
			openXMLContentFilter.setUpConfig(ParseType.MSPOWERPOINT);
		}
		else if (contentType.equals(CORE_PROPERTIES_TYPE)) {
			parseType = ParseType.MSWORDDOCPROPERTIES;
		}
		else if (contentType.equals(Powerpoint.COMMENTS_TYPE)) {
			parseType = ParseType.MSPOWERPOINTCOMMENTS;
		}
		openXMLContentFilter.setUpConfig(parseType);

		// Other configuration
		return new StandardPartHandler(openXMLContentFilter, getParams(), getZipFile(), entry);
	}

	private boolean isTranslatableType(String entryName, String type) {
		if (!entryName.endsWith(".xml")) return false;
		if (isStyledTextPart(entryName, type)) return true;
		if (getParams().getTranslateDocProperties() && type.equals(CORE_PROPERTIES_TYPE)) return true;
		if (getParams().getTranslateComments() && type.equals(Powerpoint.COMMENTS_TYPE)) return true;
		if (type.equals(Drawing.DIAGRAM_TYPE)) return true;
		if (getParams().getTranslatePowerpointNotes() && type.equals(Powerpoint.NOTES_TYPE)) return true;
		return false;
	}

	/**
	 * Do additional reordering of the entries for PPTX files to make
	 * sure that slides are parsed in the correct order.  This is done
	 * by scraping information from one of the rels files and the 
	 * presentation itself in order to determine the proper order, rather
	 * than relying on the order in which things appeared in the zip.
	 * @return the sorted enum of ZipEntry 
	 * @throws IOException if any error is encountered while reading the stream
	 * @throws XMLStreamException if any error is encountered while parsing the XML
	 */
	@Override
	Enumeration<? extends ZipEntry> getZipFileEntries() throws IOException, XMLStreamException {
		OpenXMLZipFile zipFile = getZipFile();
		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		List<? extends ZipEntry> entryList = Collections.list(entries);
		Collections.sort(entryList, new ZipEntryComparator(slideNames));
		return Collections.enumeration(entryList);
	}

	List<String> findSlides() throws IOException, XMLStreamException {
		OpenXMLZipFile zipFile = getZipFile();
		// XXX Not strictly correct, I should really look for the main document and then go from there
		Relationships rels = zipFile.getRelationships("ppt/_rels/presentation.xml.rels");
		Presentation pres = new Presentation(zipFile.getInputFactory(), rels);
		pres.parseFromXML(zipFile.getPartReader("ppt/presentation.xml"));
		return pres.getSlidePartNames();
	}

	/**
	 * Examine relationship information to find all layouts that are used in
	 * a slide in this document.  Return a list of their entry names, in order.
	 * @return list of entry names.
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	List<String> findSlideLayouts(List<String> slideNames) throws IOException, XMLStreamException {
		List<String> layouts = new ArrayList<String>();
		OpenXMLZipFile zipFile = getZipFile();
		for (String slideName : slideNames) {
			List<Relationships.Rel> rels =
					zipFile.getRelationshipsForTarget(slideName).getRelByType(SLIDE_LAYOUT_REL);
			if (!rels.isEmpty()) {
				layouts.add(rels.get(0).target);
			}
		}
		return layouts;
	}
}
