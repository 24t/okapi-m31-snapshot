package net.sf.okapi.filters.openxml;

import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Provides a block property factory.
 */
class BlockPropertyFactory {

    private final static int DEFAULT_EVENTS_SIZE = 2;

    /**
     * Creates a block property.
     *
     * @param events XML events
     *
     * @return A block property
     */
    static BlockProperty createBlockProperty(List<XMLEvent> events) {
        return new BlockProperty(events);
    }

    /**
     * Creates a block property.
     *
     * @param creationalParameters Creational parameters
     * @param localName            A local name
     * @param attributes           Attributes
     *
     * @return A block property
     */
    static BlockProperty createBlockProperty(CreationalParameters creationalParameters, String localName, Map<String, String> attributes) {
        List<XMLEvent> events = new ArrayList<>(DEFAULT_EVENTS_SIZE);

        List<Attribute> attributeList = new ArrayList<>(attributes.size());

        for (Map.Entry<String, String> attribute : attributes.entrySet()) {
            attributeList.add(creationalParameters.getEventFactory().createAttribute(
                    creationalParameters.getPrefix(), creationalParameters.getNamespaceUri(), attribute.getKey(), attribute.getValue()));
        }

        events.add(creationalParameters.getEventFactory().createStartElement(
                creationalParameters.getPrefix(), creationalParameters.getNamespaceUri(), localName, attributeList.iterator(), null));
        events.add(creationalParameters.getEventFactory().createEndElement(
                creationalParameters.getPrefix(), creationalParameters.getNamespaceUri(), localName));

        return new BlockProperty(events);
    }
}
