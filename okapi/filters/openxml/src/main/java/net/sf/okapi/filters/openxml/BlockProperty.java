package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.events.XMLEvent;
import java.util.List;

import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_BIDIRECTIONAL;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.eventEquals;

/**
 * Provides a block property.
 */
class BlockProperty implements Property {

    private static final QName DEFAULT_NAME = new QName("");

    private List<XMLEvent> events;

    BlockProperty(List<XMLEvent> events) {
        this.events = events;
    }

    @Override
    public List<XMLEvent> getEvents() {
        return events;
    }

    @Override
    public QName getName() {
        return null == events.get(0)
                ? DEFAULT_NAME
                : events.get(0).asStartElement().getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BlockProperty that = (BlockProperty) o;

        return eventEquals(events, that.events);
    }

    @Override
    public int hashCode() {
        return events.hashCode();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + XMLEventSerializer.serialize(getEvents()) + ")";
    }
}
