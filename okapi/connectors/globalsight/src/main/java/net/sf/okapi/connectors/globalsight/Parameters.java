/*===========================================================================
  Copyright (C) 2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.connectors.globalsight;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.TextInputPart;

public class Parameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	private static final String SERVERURL = "serverURL";
	private static final String TMPROFILE = "tmProfile";
	
	public Parameters () {
		super();
	}
	
	public Parameters (String initialData) {
		super(initialData);
	}
	
	public String getUsername () {
		return getString(USERNAME);
	}

	public void setUsername (String username) {
		setString(USERNAME, username);
	}

	public String getPassword () {
		return getString(PASSWORD);
	}

	public void setPassword (String password) {
		setString(PASSWORD, password);
	}

	public String getServerURL () {
		return getString(SERVERURL);
	}

	public void setServerURL (String serverURL) {
		setString(SERVERURL, serverURL);
	}

	public String getTmProfile () {
		return getString(TMPROFILE);
	}

	public void setTmProfile (String tmProfile) {
		setString(TMPROFILE, tmProfile);
	}

	@Override
	public void reset () {
		super.reset();
		setUsername("");
		setPassword("");
		setServerURL("http://HOST:PORT/globalsight/services/AmbassadorWebService?wsdl");
		setTmProfile("default");
	}

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(Parameters.SERVERURL,
			"Server URL", "The full URL of the TM server (e.g. http://xyz:8080/globalsight/services/AmbassadorWebService?wsdl");
		desc.add(Parameters.USERNAME,
			"User name", "The login name to use");
		desc.add(Parameters.PASSWORD,
			"Password", "The password for the given user name");
		desc.add(Parameters.TMPROFILE,
			"TM profile", "The name of the TM profile to use");
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription (ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("GlobalSight TM Connector Settings");
		desc.addTextInputPart(paramsDesc.get(Parameters.SERVERURL));
		desc.addTextInputPart(paramsDesc.get(Parameters.USERNAME));
		TextInputPart tip = desc.addTextInputPart(paramsDesc.get(Parameters.PASSWORD));
		tip.setPassword(true);
		desc.addTextInputPart(paramsDesc.get(Parameters.TMPROFILE));
		return desc;
	}

}
