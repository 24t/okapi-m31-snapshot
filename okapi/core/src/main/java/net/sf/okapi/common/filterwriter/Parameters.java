/*===========================================================================
  Copyright (C) 2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.common.filterwriter;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;

@EditorFor(Parameters.class)
public class Parameters extends StringParameters implements IEditorDescriptionProvider {	
	private static final String WRITE_ALL_PROPERTIES_AS_ATTRIBUTES = "writeAllPropertiesAsAttributes";
	private static final String ENABLE_DUPLICATE_PROPS = "enableDuplicateProps";
	private static final String PROP_VALUE_SEP = "PROP_VALUE_SEP";

	private static final String GENERATE_TU_UUID = "GENERATE_TU_UUID";
	private static final String NORMALIZE_INLINE_IDS = "NORMALIZE_INLINE_IDS";
	
	public Parameters() {
		super();
	}
	
	public void reset() {
		super.reset();
		setWriteAllPropertiesAsAttributes(false);
		setEnableDuplicateProps(false);
		setPropValueSep(", ");	
		setGenerateUUID(false);
		setNormalizeInlineIDs(false);
	}

	public boolean isWriteAllPropertiesAsAttributes() {
		return getBoolean(WRITE_ALL_PROPERTIES_AS_ATTRIBUTES);
	}
	
	public void setEnableDuplicateProps(boolean duplicateProps) {
		setBoolean(ENABLE_DUPLICATE_PROPS, duplicateProps);
	}
	
	public boolean isEnableDuplicateProps() {
		return getBoolean(ENABLE_DUPLICATE_PROPS);
	}
	
	public void setWriteAllPropertiesAsAttributes(boolean writeAllPropertiesAsAttributes) {
		setBoolean(WRITE_ALL_PROPERTIES_AS_ATTRIBUTES, writeAllPropertiesAsAttributes);
	}
	
	public String getPropValueSep () {
		return getString(PROP_VALUE_SEP);
	}
	
	public void setPropValueSep (String sep) {
		setString(PROP_VALUE_SEP, sep);
	}
	
	public boolean isGenerateUUID() {
		return getBoolean(GENERATE_TU_UUID);
	}
	
	public void setGenerateUUID(boolean generateUUID) {
		setBoolean(GENERATE_TU_UUID, generateUUID);
	}
	
	public boolean isNormalizeInlineIDs() {
		return getBoolean(NORMALIZE_INLINE_IDS);
	}
	
	public void setNormalizeInlineIDs(boolean normalizeInlineIDs) {
		setBoolean(NORMALIZE_INLINE_IDS, normalizeInlineIDs);
	}
	
	@Override
	public ParametersDescription getParametersDescription() {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(WRITE_ALL_PROPERTIES_AS_ATTRIBUTES, 
				"Write all text unit level properties as TMX attributes", 
				null);
		desc.add(ENABLE_DUPLICATE_PROPS, 
				"Expand out duplicate property values into individual elements", 
				null);
		desc.add(PROP_VALUE_SEP, 
				"Seperator string used to delimit duplicate property values", 
				null);
		desc.add(GENERATE_TU_UUID, 
				"Generate a UUID instead of an auto generated id or existing name", 
				null);
		desc.add(NORMALIZE_INLINE_IDS, 
				"Start inline code ids at 1 for each segment", 
				null);
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription(ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("TMX Filter Writer Options", true, false);
		desc.addCheckboxPart(paramsDesc.get(WRITE_ALL_PROPERTIES_AS_ATTRIBUTES));
		desc.addCheckboxPart(paramsDesc.get(ENABLE_DUPLICATE_PROPS));
		desc.addTextInputPart(paramsDesc.get(PROP_VALUE_SEP));
		desc.addCheckboxPart(paramsDesc.get(GENERATE_TU_UUID));
		desc.addCheckboxPart(paramsDesc.get(NORMALIZE_INLINE_IDS));
		return desc;
	}
}
