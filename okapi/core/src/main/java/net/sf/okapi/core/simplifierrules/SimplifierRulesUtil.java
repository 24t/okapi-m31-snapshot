package net.sf.okapi.core.simplifierrules;

public class SimplifierRulesUtil {
	public final static String unescape(String t) {
		String unescaped = t.replace("\\n", "\n");
		unescaped = unescaped.replace("\\r", "\r");
		unescaped = unescaped.replace("\\t", "\t");
		unescaped = unescaped.replace("\\\\", "\\");
		unescaped = unescaped.replace("\\\"", "\"");
		return unescaped;
	}
}
