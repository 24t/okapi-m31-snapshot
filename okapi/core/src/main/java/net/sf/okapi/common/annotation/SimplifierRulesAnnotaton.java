package net.sf.okapi.common.annotation;

public class SimplifierRulesAnnotaton implements IAnnotation {
	private String rules;

	public String getRules() {
		return rules;
	}

	public void setRules(String rules) {
		this.rules = rules;
	}
}
