package net.sf.okapi.common.resource;

/**
 * Provides extended code types.
 */
public enum ExtendedCodeType {

    COLOR("color"),
    HIGHLIGHT("highlight"),
    SHADE("shade"),
    SHADOW("shadow"),
    STRIKE_THROUGH("strikethrough"),
    UNDERLINE("underline");

    String value;

    ExtendedCodeType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
