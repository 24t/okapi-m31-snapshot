package net.sf.okapi.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class StreamUtilTest {

	@Test
	public void testCRC() {
		FileLocation location = FileLocation.fromClass(this.getClass());
		assertEquals(1018854380L, StreamUtil.calcCRC(location.in("/ParamTest01.txt").asInputStream()));
		assertEquals(3995389683L, StreamUtil.calcCRC(location.in("/safeouttest1.txt").asInputStream()));
		assertEquals(369693688L, StreamUtil.calcCRC(location.in("/test_path1.txt").asInputStream()));
		assertEquals(681066369L, StreamUtil.calcCRC(location.in("/test.html").asInputStream()));
	}
	
	@Test
	public void streamAsString() throws Exception {
	   File tmp = File.createTempFile("test", ".txt");
	   assertTrue(tmp.exists());
	   // bis is closed twice by try-with-resource and streamAsString, but this shows best practice of using
	   // BOMAwareInputStream
	   try (BOMAwareInputStream bis = new BOMAwareInputStream(new FileInputStream(tmp), "UTF-8")) {
		   StreamUtil.streamAsString(bis, bis.detectEncoding());
	   }
	   assertTrue("Can't delete " + tmp.getAbsolutePath(), tmp.delete());
	}
}
