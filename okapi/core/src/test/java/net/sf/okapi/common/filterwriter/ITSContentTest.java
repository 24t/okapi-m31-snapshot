/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.common.filterwriter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import net.sf.okapi.common.annotation.GenericAnnotation;
import net.sf.okapi.common.annotation.GenericAnnotationType;
import net.sf.okapi.common.annotation.GenericAnnotations;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ITSContentTest {

	@Test
	public void extendedMatchTest () {
		assertTrue(ITSContent.isExtendedMatch("*-CH", "de-CH"));
		assertTrue(ITSContent.isExtendedMatch("*-CH", "fr-ch"));
		assertTrue(ITSContent.isExtendedMatch("de-*-DE, fr", "de-de"));
		assertTrue(ITSContent.isExtendedMatch("fr, de-*-DE", "de-Latn-de"));
		assertTrue(ITSContent.isExtendedMatch("za ,  de-*-DE , po-pl  ", "de-DE-x-goethe"));
		assertTrue(ITSContent.isExtendedMatch("za, de-*-DE,   po-pl", "de-DE"));
		
		assertTrue(ITSContent.isExtendedMatch("za, de-*, po-pl", "de"));
		assertTrue(ITSContent.isExtendedMatch("de-*, de", "de"));
		
		assertTrue(ITSContent.isExtendedMatch("de-*-DE", "de-DE"));
		assertTrue(ITSContent.isExtendedMatch("de-*-DE", "de-de"));
		assertTrue(ITSContent.isExtendedMatch("de-*-DE", "de-Latn-DE"));
		assertTrue(ITSContent.isExtendedMatch("de-*-DE", "de-Latf-DE"));
		assertTrue(ITSContent.isExtendedMatch("de-*-DE", "de-DE-x-goethe"));
		assertTrue(ITSContent.isExtendedMatch("de-*-DE", "de-Latn-DE-1996"));
		assertTrue(ITSContent.isExtendedMatch("de-*-DE", "de-Deva-DE"));
		
		assertFalse(ITSContent.isExtendedMatch("de-*-DE", "de"));
		assertFalse(ITSContent.isExtendedMatch("de-*-DE", "de-x-DE"));
		assertFalse(ITSContent.isExtendedMatch("de-*-DE", "de-Deva"));

		assertFalse(ITSContent.isExtendedMatch("", "fr"));
		assertTrue(ITSContent.isExtendedMatch("*", "fr"));
	}

	@Test
	public void testGetAnnotatorRef () {
		ITextUnit tu = new TextUnit("id");
		GenericAnnotation ann = new GenericAnnotation(GenericAnnotationType.ANNOT,
			GenericAnnotationType.ANNOT_VALUE, "translate|uri1"); 
		GenericAnnotations.addAnnotations(tu, new GenericAnnotations(ann));
		assertEquals("uri1", ITSContent.getAnnotatorRef("translate", tu));
		assertEquals(null, ITSContent.getAnnotatorRef("invalidDataCategory", tu));
		
		ann.setString(GenericAnnotationType.ANNOT_VALUE, "mt-confidence|uri2 ");
		assertEquals("uri2", ITSContent.getAnnotatorRef("mt-confidence", tu));

		ann.setString(GenericAnnotationType.ANNOT_VALUE, "mt-confidence|uri3 text-analysis|uri4 translate|uri5");
		assertEquals("uri3", ITSContent.getAnnotatorRef("mt-confidence", tu));
		assertEquals("uri4", ITSContent.getAnnotatorRef("text-analysis", tu));
		assertEquals("uri5", ITSContent.getAnnotatorRef("translate", tu));

		ann.setString(GenericAnnotationType.ANNOT_VALUE, "domain| text-analysis|");
		assertEquals("", ITSContent.getAnnotatorRef("domain", tu));
		assertEquals("", ITSContent.getAnnotatorRef("text-analysis", tu));
	}
}
