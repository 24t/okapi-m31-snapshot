/*===========================================================================
  Copyright (C) 2009-2012 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.applications.rainbow.pipeline;

/**
 * Maps the class of a pipeline step to its UI dependencies. 
 * Specifically, the editor class and the description provider class, both
 * of which are null, if not set. All fields are final and read-only.
 * 
 * @author Martin Wunderlich
 */
public class PipelineStepUIDescription {
	private final String pipelineStepClass;
	private final String editorClass;
	private final String descriptionProviderClass;
	
	public PipelineStepUIDescription(String pipelineStepClass) {
		this.pipelineStepClass = pipelineStepClass;
		this.editorClass = null;
		this.descriptionProviderClass = null;
	}
	
	public PipelineStepUIDescription(String pipelineStepClass, String editorClass, String descriptionProviderClass) {
		this.pipelineStepClass = pipelineStepClass;
		this.editorClass = editorClass;
		this.descriptionProviderClass = descriptionProviderClass;
	}
	
	public String getPipelineStepClass() {
		return pipelineStepClass;
	}

	public String getEditorClass() {
		return editorClass;
	}

	public String getDescriptionProviderClass() {
		return descriptionProviderClass;
	}

	public boolean hasDescriptionProvider() {
		return descriptionProviderClass != null;
	}

	public boolean hasEditor() {
		return editorClass != null;
	}
}
