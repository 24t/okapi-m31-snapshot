#!/bin/bash

JAVA_HOME="$(/usr/libexec/java_home -v 1.7+ -F)"

if [ -z "$JAVA_HOME" ]; then
    echo "Okapi requires Java 1.7 or higher."
    exit 1
fi

JAVA="$JAVA_HOME/bin/java"

"$JAVA" -d64 -XstartOnFirstThread -jar "$(dirname "$0")/lib/tikal.jar" "$@"
